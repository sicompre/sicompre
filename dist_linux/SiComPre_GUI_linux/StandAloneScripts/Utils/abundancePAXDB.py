from __future__ import division

import optparse
import sys
import _mysql
import operator
from scipy.stats import hypergeom

from textwrap import dedent



def canonical_protein_name(name):
    """Returns the canonical name of a protein by performing a few simple
    transformations on the name."""
    return name.strip().upper()

def is_numeric(x):
    """Returns whether the given string can be interpreted as a number."""
    try:
        float(x)
        return True
    except:
        return False

def matching_score(set1, set2):
    return len(set1.intersection(set2))**2 / (float(len(set1)) * len(set2))
    
def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


def fraction_matched(reference, predicted, predicted2, abd, edges, edgesF, funschema, strategy, corum, idcorum, score_threshold=0.25):
    result = 0
    protAbd = {}
    tot_mol=0
    avg={}
    abd_a = {}
    abd_b = {}
    for id1, c1 in enumerate(reference):
        protAbd[id1] = {}
        abd_a[id1] = 0
        abd_b[id1] = 0
        avg[id1]=0
        for prot in c1:
            protAbd[id1][prot] = 0
    for id2, c2 in enumerate(predicted):
        maxs=0
        matchedID=-1
        for id1, c1 in enumerate(reference):
            score = matching_score(c1, c2)
            if score>=maxs:
               maxs=score
               matchedID=id1
        abd[matchedID]+=1
        abd_a[matchedID]+=1
        avg[matchedID]+=len(c2)
        #print matchedID
        #print reference[matchedID]
        for prot in c2:
            if prot in reference[matchedID]:
               #print prot
               protAbd[matchedID][prot]+=1
            #if score > score_threshold:
            #    result += 1
            #    tot_mol+=len(c1)
            #    abd[id1]+=1
    for id2, c2 in enumerate(predicted2):
        maxs=0
        matchedID=-1
        for id1, c1 in enumerate(reference):
            score = matching_score(c1, c2)
            if score>=maxs:
               maxs=score
               matchedID=id1
        abd[matchedID]+=1
        abd_b[matchedID]+=1
        avg[matchedID]+=len(c2)
        #print matchedID
        #print reference[matchedID]
        for prot in c2:
            if prot in reference[matchedID]:
               #print prot
               protAbd[matchedID][prot]+=1
            #if score > score_threshold:
            #    result += 1
            #    tot_mol+=len(c1)
            #    abd[id1]+=1
    #print nodes
    db=_mysql.connect("localhost","root","root","yeastPPI")
    db.query("SELECT distinct(name) as name FROM human_reference_compl_corum_cell")
    r=db.store_result()
    res=r.fetch_row(maxrows=0,how=1)
    ref = []
    rid=0
    idToName = {}
    for row in res:
        db.query("SELECT prot FROM human_reference_compl_corum_cell where name = '"+addslashes(row['name'])+"'")
        r2=db.store_result()
        res2=r2.fetch_row(maxrows=0,how=1)
        ref2 = []
        idToName[rid]=row['name']
        for row2 in res2:
            ref2.append(row2['prot'].replace('\r', ''))
        ref.append(ref2)
        #print len(ref2)
        rid+=1
    flag=False
    #print ref
    db.query("SELECT COUNT(Distinct(IDprot)) as c FROM human_functions")
    r=db.store_result()
    res=r.fetch_row(maxrows=0,how=1)
    NN=0
    for row in res:
        NN=row['c']
    
    db.query("SELECT fun, COUNT(Distinct(IDprot)) as c FROM human_functions GROUP by fun")
    r=db.store_result()
    res=r.fetch_row(maxrows=0,how=1)
    m={}
    for row in res:
        m[row['fun']]=row['c']

    print 'Complex ID\tSize\tList of proteins\tQuantitative prediction\tMin abundance\tMax abundance\tAverage size of simulated complexes\tBest matching reference complex\tMatching thr\tLength of reference complex\tFraction of fictitious domains\tFunctional Enrichment'
    for id1, c1 in enumerate(reference):
        tot_int=0
        tot_f=0
        flag=False
        if ((abd[id1]/2)>=6 or len(c1)<=16) and strategy=='c':
           flag=True
        elif ((abd[id1]/2)>=3 or len(c1)>=3) and strategy=='f':
           flag=True
        elif (strategy==''):
           flag=True
        if flag:
          print ''+str(id1)+'\t'+str(len(c1))+'\t',
          for prot in c1:
              print prot+',',
          print '\t'+str(abd[id1]/2)+'\t',
          if abd_a[id1]<abd_b[id1]:
             print str(abd_a[id1])+'\t'+str(abd_b[id1])+'\t',
          else:
             print str(abd_b[id1])+'\t'+str(abd_a[id1])+'\t',
          if abd[id1]!=0:
             print "%.4f" % (avg[id1]/abd[id1]),
          else:
               print '0',
          #print ''
          for prot in c1:
              #check edges
              if prot in edges:
                 for prot2 in c1:
                    if prot2 in edges[prot]:
                      tot_int+=1
                      if prot in edgesF:
                         if prot2 in edgesF[prot]:
                            tot_f+=1
          #print str(tot_int)+'-'+str(tot_f)
          maxs=-1
          mlen=0
          for id2, c2 in enumerate(ref):
              #print c2
              score = matching_score(c1, c2)
              if score>=maxs:
                 maxs=score
                 matchedID=id2
                 mlen=len(c2)
          if maxs>0:
             print '\t'+idToName[matchedID]+'\t',
             print "%.4f" % (maxs),
             print "\t"+str(mlen),
          else:
               print '\t\t\t',
          maxs=-1
          mlen=0
          for id2, c2 in enumerate(corum):
              #print c2
              score = matching_score(c1, c2)
              if score>=maxs:
                 maxs=score
                 matchedID=id2
                 mlen=len(c2)
          if maxs>0:
             print '\t'+idcorum[matchedID]+'\t',
             print "%.4f" % (maxs),
             print "\t"+str(mlen),
          else:
               print '\t\t\t',
          print '\t'+str(tot_f/tot_int),
          n=len(c1)
          fun = {}
          tot=0
          for prot in c1:
            db.query("SELECT fun FROM human_functions WHERE IDprot='"+prot+"'")
            r=db.store_result()
            res=r.fetch_row(maxrows=0,how=1)
            for row in res:
                if row['fun'] in fun:
                   fun[row['fun']]+=1
                   tot+=1
                else:
                   fun[row['fun']]=1
                   tot+=1
          print '\t',
          pvalue={}
          for f in fun:
              k=fun[f]
              pvalue[f]=hypergeom.pmf(k,int(NN),int(m[f]),n)
              #print hypergeom.sf(1,6128,216,62)

          sorted_x = sorted(pvalue.iteritems(), key=operator.itemgetter(1))
          #sorted_x.reverse()
          fs=[]
          for k,v in sorted_x:
              fs.append(k)
          sorted_x=fs

          for f in sorted_x:
              if pvalue[f]<=0.1:
                print str(funschema[f]).replace('\n', '')+':',
                print "%.4g\t" % (pvalue[f]),
                #print str(pvalue[f])+',',
                #print str(fun[f])+'\t'+str(NN)+'\t'+str(m[f])+'\t',
          print ''


        #if abd[id1]>1:
        #   print c1

    return result / len(reference)

def read_complexes(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
    result = []
    for line in open(fname):
        ps = set(canonical_protein_name(x) for x in line.strip().split() if x)
        if known_proteins is not None:
            isect = ps.intersection(known_proteins)
            if len(isect) < max(min_size, len(ps) * strictness):
                continue
            if len(isect) > max_size:
                continue
            ps = isect
        result.append(ps)

    to_delete = set()
    for idx, cluster in enumerate(result):
        for idx2, cluster2 in enumerate(result):
            if (idx == idx2) or ((idx2 in to_delete) or (idx in to_delete)):
                continue
            if cluster == cluster2:
                to_delete.add(idx2)

    result = [r for i, r in enumerate(result) if i not in to_delete]
    return result
def read_complexes2(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
    result = []
    for line in open(fname):
        ps = set(canonical_protein_name(x) for x in line.strip().split() if x)
        if known_proteins is not None:
            isect = ps.intersection(known_proteins)
            if len(isect) < max(min_size, len(ps) * strictness):
                continue
            if len(isect) > max_size:
                continue
            ps = isect
        result.append(ps)

    to_delete = set()
    for idx, cluster in enumerate(result):
        for idx2, cluster2 in enumerate(result):
            if (idx == idx2) or ((idx2 in to_delete) or (idx in to_delete)):
                continue
            if cluster == cluster2:
                to_delete.add(idx2)

    #result = [r for i, r in enumerate(result) if i not in to_delete]
    return result
    
def read_complexes3(corum_names, fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
    result = []
    counter=0
    for line in open(fname):
        if counter%2==0:
           #get name
           corum_names[counter/2]=line.replace('\n', '').replace('\r', '')
        else:
          ps = set(canonical_protein_name(x) for x in line.strip().split() if x)
          if known_proteins is not None:
              isect = ps.intersection(known_proteins)
              if len(isect) < max(min_size, len(ps) * strictness):
                  continue
              if len(isect) > max_size:
                  continue
              ps = isect
          result.append(ps)
        counter+=1

    to_delete = set()
    for idx, cluster in enumerate(result):
        for idx2, cluster2 in enumerate(result):
            if (idx == idx2) or ((idx2 in to_delete) or (idx in to_delete)):
                continue
            if cluster == cluster2:
                to_delete.add(idx2)

    #result = [r for i, r in enumerate(result) if i not in to_delete]
    return result


def read_network(self, fname):
    known_proteins = set()
    for line in open(fname):
        parts = [canonical_protein_name(part) for part in line.strip().split()
                if not is_numeric(part)]
        known_proteins.update(parts)
    return known_proteins
    
def read_matrix(fname):
    mapping = {}
    for line in open(fname):
        prots=line.split('\t')
        #print line
        prots[1]=prots[1].replace('\n', '').replace('\r', '')
        if prots[0] in mapping:
           mapping[prots[0]].append(prots[1])
        else: 
              mapping[prots[0]]=[prots[1]]

        #known_proteins.update(parts)
    return mapping

def read_abd(fname, mapping):
    unipAbd = {}
    for line in open(fname):
        words = line.split('\t',1)

        words[1]=words[1].replace('\n', '').replace('\r', '')
        if words[0] in mapping:
           for prot in mapping[words[0]]:
               if prot in unipAbd:
                  unipAbd[prot].append(words[1])
               else:
                    unipAbd[prot]=[words[1]]
               #print prot+"\t"+words[1]

    for p in unipAbd:
        avg=0
        for a in unipAbd[p]:
            avg+=float(a)
        avg=avg/len(unipAbd[p])
        print p+"\t"+str(avg)
        
    return 0


def abd(argv):
    #self.options, self.args = self.parser.parse_args()

    #if not self.options.measures:
    #    self.options.measures = sorted(self.measures.keys())

    #if self.options.network:
    #    known_proteins = self.read_network(self.options.network)
    #    self.log("%d known proteins found in network" % len(known_proteins))
    #else:
    #print hypergeom.sf(1,6128,216,62)

    mapping = read_matrix(sys.argv[1])
    abundances = read_abd(sys.argv[2],mapping)
    #edges={}
    #edgesF={}


    return 0


def main(argv):
    return abd(argv)


if __name__ == "__main__":
   main(sys.argv[1:])
