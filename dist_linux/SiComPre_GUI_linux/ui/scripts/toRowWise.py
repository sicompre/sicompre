from __future__ import division

import optparse
import sys
from textwrap import dedent


def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False
	
def toRowWise():
	for i,file in enumerate(sys.argv):
		if i>0:
			out_file = open(file+"2","w")
			for line in open(file):
				if line[0]=='#':
					line=line.replace('\n', '').replace('\r', '')
					complexID=int(line[1:])
					if complexID!=0 and contains_prots:
						out_file.write('\n')
					contains_prots=False
				elif line[0]!='#' and (not "Execution" in line):
					line=line.replace('\n', '').replace('\r', '')
					if not isfloat(line) and complexID!=0:
						prots=line.split(' ')
						out_file.write(prots[0]+" ")
						contains_prots=True
			out_file.write('\n')
			out_file.flush()
			out_file.close()
		

