'''
Executes the SiComPre pipeline to predict protein complexes
developed by The Microsoft Research - University of Trento Centre for Computational and Systems Biology (COSBI)
www.cosbi.eu/prototype/sicompre


example:

'''

from __future__ import division

import optparse
import sys
from textwrap import dedent
import os
import platform
import re
import subprocess
import model_generation
import toRowWise
import merging
import generateCSV_20

SCRIPTS_PATH = ""
GPU_ID = 0	#set it to -1 if you don't want to use GPU computing

currOS=platform.system() #'Windows' 'Linux'
argvCSV=[]
file=""

def split(TPs):
	TPs=TPs.split(",")
	merged=open("merged.txt",'w')
	global argvCSV
	for tp in TPs:
		if tp=="last":
			filename="complex_structure.out"
		else:
			filename="complex_structure%0.6f.out" % float(tp)
		print "splitting file "+filename
		
		tmp_file2=open("tmp_file2.txt",'wb')
		for line in open(filename):
			#line=line.replace(",","\t").replace("\r","")
			prots=line.split(',')
			print '$'+prots[0]+"\t"+prots[1]+'$'
			tmp_file2.write(prots[0]+"\t"+prots[1]+"\n")
		tmp_file2.flush()
		tmp_file2.close()
		
		if currOS=="Linux":
			#os.system('..\..\mcl tmp_file2.txt --abc -I 1.4 -o tmp_file.txt')
			cmd = ['..\..\mcl', 'tmp_file2.txt', '--abc', '-I', '1.4', '-o', 'tmp_file.txt']
			p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
			for line in p.stdout:
				print line
			p.wait()
			print p.returncode
		elif currOS=="Windows":
			#os.system('..\..\MCL.exe tmp_file2.txt --abc -I 1.4 -o tmp_file.txt')
			cmd = ['..\..\MCL.exe', 'tmp_file2.txt', '--abc', '-I', '1.4', '-o', 'tmp_file.txt']
			p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
			for line in p.stdout:
				print line
			p.wait()
			print p.returncode
			
		tmp_file=open(filename+'.mcl','w')
		argvCSV.append(file+"_Sim"+"\\"+str(n_iter+1)+"\\"+filename+".mcl")
		for line in open("tmp_file.txt"):
			line = re.sub(r'_[0-9]+', r'', line.rstrip())
			tmp_file.write(line+"\n")
			merged.write(line+"\n")
		tmp_file.flush()
		tmp_file.close()
		merged.flush()
	merged.close()
	

print ">>>Model generation"
model_generation.modelGeneration(sys.argv) #generate_model
n_iter=int(sys.argv[-1]) #number of simulations to run

#get filename of the model and of the whole simulation
if sys.argv[5]=="none":	
	file=sys.argv[11]
else:
	file=sys.argv[12]
	
try:
	if sys.argv[5]=="none":
		os.makedirs(file+"_Sim") #create simulation directory
	else:
		os.makedirs(file+"_Sim") #create simulation directory
except:
	print "simulation directory already exists, resuming simulations..."
	
#argvCSV=['',file+'_Sim'+'\\refined_complexes.txt',sys.argv[1],'fictitious_int.txt',sys.argv[-4],sys.argv[-5]]
argvCSV=['',file+'_Sim'+'\\refined_complexes.txt',sys.argv[1],'fictitious_int.txt',sys.argv[-4]]
os.chdir(file+"_Sim")
refinedC=open("refined_complexes.txt",'w')
for n_iter in range(n_iter):
	print ">>>Simulation "+str(n_iter+1)
	try:
		os.makedirs(str(n_iter+1))
	except:
		print "error creating directory"
	os.chdir(str(n_iter+1))
	if GPU_ID>=0:
		print '..\..\SiComPre20.exe -f ..\..\\'+file+' -c 0.1 -ct 16 -mc '+sys.argv[-5]
		#os.system('..\..\SiComPre20.exe -f ..\..\\'+file+' -c 0.1 -ct 16 -mc '+sys.argv[-5])
		cmd = ['..\..\SiComPre20.exe', '-f', '..\..\\'+file, '-c', '0.1', '-ct', '16', '-mc', sys.argv[-5]]
		#cmd = ['..\..\SiComPre20.exe']
		p = subprocess.Popen('..\..\SiComPre20.exe -f ..\..\\'+file+' -c 0.1 -ct 16 -mc '+sys.argv[-5], stdout=subprocess.PIPE)
		for line in p.stdout:
			print line,
		p.wait()
		print p.returncode
	else:
		print '..\..\SiComPre20.exe -f ..\..\\'+file+' -c 0.1 -ct 16 -mc '+sys.argv[-5]+' -noGPU'
		#os.system('..\..\SiComPre20.exe -f ..\..\\'+file+' -c 0.1 -ct 16 -mc '+sys.argv[-5]+' -noGPU')
		#cmd = ['..\..\SiComPre20.exe', '-f ..\..\\'+file, '-c', 0.1', '-ct', '16', '-mc', sys.argv[-5], '-noGPU']
		p = subprocess.Popen('..\..\SiComPre20.exe -f ..\..\\'+file+' -c 0.1 -ct 16 -mc '+sys.argv[-5]+' -noGPU', stdout=subprocess.PIPE)
		for line in p.stdout:
			print line
		p.wait()
		print p.returncode
	split(sys.argv[-2])
	print ">>>Merging with thr"+str(sys.argv[-3])
	merging.merging(['','merged.txt'])
	for line in open("refined_complexes.txt"):
		refinedC.write(line)
	refinedC.flush()
	os.chdir('..')
print ">>>generating quantitative prediction"
refinedC.close()
merging.merging(['','refined_complexes.txt'])
print ">>>Generating CSV"
os.chdir('..')
#os.chdir(sys.argv[10])
so=sys.stdout
sys.stdout=open(file+'_Sim'+'\prediction.csv','w')
generateCSV_20.generateCSV(argvCSV)
sys.stdout=so
print "Completed!"
print "Check results in the file "+str(file)+"\prediction.csv"


