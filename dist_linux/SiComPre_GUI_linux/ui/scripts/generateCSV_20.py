from __future__ import division

#import _mysql
import operator
import optparse
from scipy.stats import hypergeom
#import sys
from textwrap import dedent
#import urllib2
from wx.lib.pubsub import pub
import wx


def canonical_protein_name(name):
    """Returns the canonical name of a protein by performing a few simple
    transformations on the name."""
    return name.strip().upper()

def is_numeric(x):
    """Returns whether the given string can be interpreted as a number."""
    try:
        float(x)
        return True
    except:
        return False

def matching_score(set1, set2):
    return len(set1.intersection(set2))**2 / (float(len(set1)) * len(set2))
    
def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


def fraction_matched(reference, predicted, abd, edges, edgesF, corum, idcorum,tot_timepoints, allProts, output_file, score_threshold=0.25):
    result = 0
    avg={}
    abd = {}
    outputf=open(output_file,'w')
    for id1, c1 in enumerate(reference):
    	avg[id1]=0
    for i in predicted:
    	abd_t = {}
    	for id1, c1 in enumerate(reference):
    		abd_t[id1] = 0
    
    	for id2, c2 in enumerate(predicted[i]):
    		maxs=0
    		matchedID=-1
    		matchedC=[]
    		for id1, c1 in enumerate(reference):
    			score = matching_score(c1, c2)
    			if score>=maxs:
    			   maxs=score
    			   matchedID=id1
    			   matchedC=c1
    		abd_t[matchedID]+=1
    		avg[matchedID]+=len(c2)
    	abd[i]=abd_t
    outputf.write('Complex ID;Size;List of proteins;')
    for i in predicted:
    	outputf.write('Quantitative prediction '+str(i)+';')
    outputf.write("Average abundance;Average size of simulated complexes;Best matching reference complex;Matching thr;Length of reference complex;P-value matching;Fraction of fictitious domains;Functional Enrichment (DAVID)\n")
    for id1, c1 in enumerate(reference):
    	tot_int=0
    	tot_f=0
    	outputf.write(str(id1)+';'+str(len(c1))+';')
    	protL=""
    	for prot in c1:
    		outputf.write(prot+","),
    		protL=str(protL)+str(prot)+","
    	outputf.write(';'),
    	protL=protL[0:len(protL)-1]
    	tot_abd=0.0
    	for i in predicted:
    		tot_abd+=abd[i][id1]
    		outputf.write(str(abd[i][id1])+';')
    	if tot_abd!=0:
    		outputf.write("%.4f" % (tot_abd/(tot_timepoints)))
    		outputf.write(";")
    		outputf.write("%.4f" % (avg[id1]/tot_abd))
    	else:
    		outputf.write('0')
    		outputf.write(";")
    		outputf.write('0')
    	for prot in c1:
    		#check edges
    		if prot in edges:
    			for prot2 in c1:
    				if prot2 in edges[prot]:
    					tot_int+=1
    				if prot in edgesF:
    					if prot2 in edgesF[prot]:
    						tot_f+=1
    	maxs=-1
    	mlen=0
    	for id2, c2 in enumerate(corum):
    		score = matching_score(c1, c2)
    		if score>=maxs:
    			maxs=score
    			matchedID=id2
    			mlen=len(c2)
    	if maxs>0:
    		outputf.write(';'+idcorum[matchedID]+';')
    		outputf.write("%.4f" % (maxs))
    		outputf.write(";"+str(mlen))
    		outputf.write(";")
    		k=len(set(corum[matchedID]).intersection(set(c1)))
    		outputf.write(str(hypergeom.sf(k-1,len(allProts),len(set(corum[matchedID])),len(c1))))
    	else:
    		outputf.write(';;;;')
        if tot_int>0:
            outputf.write(';'+str(tot_f/tot_int)+';')
        else:
            outputf.write(';0;')
    	n=len(c1)
    	fun = {}
    	tot=0
    	outputf.write("http://david.abcc.ncifcrf.gov/api.jsp?type=ENSEMBL_GENE_ID&ids="+str(protL)+"&tool=chartReport&annot=GOTERM_MF_ALL")
    	outputf.write("\n")
        outputf.flush()
    outputf.close()
    return result / len(reference)

def read_complexes(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)

		result.append(ps)
		
	return result

	
def read_complexes2(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	complexes={}
	j=0
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)		
		ps2=[]
		compart=[]
		for prot in ps:
			if '_' in prot:
				ps2.append(prot.split('_')[0])
				compart.append(prot.split('_')[1])
			else:
				ps2.append(prot)
		ps=set(ps2)
		compart=set(compart)
		complexes[j]=(ps,compart)
		j+=1
		if known_proteins is not None:
			isect = ps.intersection(known_proteins)
			if len(isect) < max(min_size, len(ps) * strictness):
				continue
			if len(isect) > max_size:
				continue
			ps = isect
		result.append(ps)

	return result,complexes
    
def read_complexes3(corum_names, fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	counter=0
	compls={}
	for line in open(fname):
		words=line.split('\t')
		words[1]=words[1].replace('\n', '').replace('\r', '')
		if words[1] in compls:
			compls[words[1]].append(words[0])
		else:
			compls[words[1]]=[words[0]]
			
	for i,c in enumerate(compls):
		corum_names[i]=c
		result.append(compls[c])
		
	return result


def read_network(self, fname):
    known_proteins = set()
    for line in open(fname):
        parts = [canonical_protein_name(part) for part in line.strip().split()
                if not is_numeric(part)]
        known_proteins.update(parts)
    return known_proteins
    
def read_matrix(fname):
    edges = {}
    for line in open(fname):
        prots=line.split('\t')
        #print line
        prots[1]=prots[1].replace('\n', '').replace('\r', '')
        if prots[0] in edges:
             if prots[1] in edges[prots[0]]:
                edges[prots[0]][prots[1]]+= 1
             else:
                  edges[prots[0]][prots[1]] = 1
        else:
             edges[prots[0]] = {}
             edges[prots[0]][prots[1]] = 1

        if prots[1] in edges:
             if prots[0] in edges[prots[1]]:
                edges[prots[1]][prots[0]]+= 1
             else:
                  edges[prots[1]][prots[0]] = 1
        else:
             edges[prots[1]] = {}
             edges[prots[1]][prots[0]] = 1
        #known_proteins.update(parts)
    return edges

def read_fun(fname):
    fun = {}
    for line in open(fname):
        words = line.split(' ',1)
        fun[words[0]] = words[1]
    return fun

def generateCSV(argv,progressBar,progressCounter,sessionDir):
    
    #example: python generateCSV_20.py ..\refined.txt ..\collins_int.txt ..\fictitious_interactions.txt ..\funcat.txt t ..\CYC08_C1.txt ..\resultCOMPLEX.out2 > test.csv	
    
    reference_complexes = read_complexes(argv[1])
    edges = read_matrix(argv[2])
    edgesF = read_matrix(argv[3])
    
    idToName2 = {}
    corum=[]
    if argv[4]!="":
        corum = read_complexes3(idToName2,argv[4])
    progressCounter+=1
    print "pg:"+str(progressCounter)
    wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)

	#corumOriginal = read_complexes3(idToName2,sys.argv[7])

	#if strategy=='n':
	#	strategy=''
    abdd = {}
    for i, c1 in enumerate(reference_complexes):
    	abdd[i]=0
    
    predicted_complexes={}
    pc={}
    tot_timepoints=0
    for i,f in enumerate(argv):
    	if i>4:
            progressCounter+=1
            wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
            predicted_complexes2,pc2 = read_complexes2(argv[i])
            predicted_complexes[i-4]=predicted_complexes2
            pc=dict(pc.items() + pc2.items())
            tot_timepoints+=1
    allProts=[]
    for i in predicted_complexes:
    	for c in predicted_complexes[i]:
    		allProts=allProts+list(c)
    allProts=list(set(allProts))
    progressCounter+=1
    print "pg:"+str(progressCounter)
    wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
    fraction_matched(reference_complexes, predicted_complexes, abdd, edges, edgesF, corum,idToName2,tot_timepoints,allProts,sessionDir+"/prediction.csv")
    progressCounter+=1
    print "pg:"+str(progressCounter)
    wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
    
    return 0


