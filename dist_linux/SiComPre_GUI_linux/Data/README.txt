Input data used by SiComPre
----------------------------------------------
Files contained in this directory have been generated using the following sources:

Yeast
- PPI from Collins et al. Mol Cell Proteomics (2007)
- Protein Abundances from Ghaemmaghami, Nature, 2003
- GO functions and localization downloaded from MIPS (Mewes et al. Nucleic Acids Res. 2006)
- reference protein complexes from CYC08 (Pu et al. Nucleic Acids Res. 2009)
- DDI from IDDI (Kim et al. Proteome Science, 2012) 

Mouse
- PPI data from Biogrid (Stark et al. Nucleic Acids Res. 2006)
- Protein Abundances downloaded from MOPED DB (Kolker et al. Nucleic Acids Res. 2012)
- GO functions and localization downloaded querying GO database in October 2014 
- reference protein complexes from CORUM DB (Ruepp et al. Nucleic Acids Res. 2009)
- DDI from IDDI (Kim et al. Proteome Science, 2012)
- Metformin-Protein interactions from DrugBank (Wishart et al. Nucleic Acids Res. 2006)

Human
- PPI data from Hippie (Schaefer et al. Plos One, 2012)
- Protein Abundances downloaded from MOPED DB (Kolker et al. Nucleic Acids Res. 2012) and (Kim et al. Nature, 2014)
- GO functions and localization downloaded querying GO database in October 2014 
- reference protein complexes from CORUM DB (Ruepp et al. Nucleic Acids Res. 2009)
- DDI from IDDI (Kim et al. Proteome Science, 2012)
