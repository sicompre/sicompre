from __future__ import division

import optparse
import sys

from textwrap import dedent


protAbd={}

def canonical_protein_name(name):
    """Returns the canonical name of a protein by performing a few simple
    transformations on the name."""
    return name.strip().upper()

def is_numeric(x):
    """Returns whether the given string can be interpreted as a number."""
    try:
        float(x)
        return True
    except:
        return False

def matching_score(set1, set2):
    return len(set1.intersection(set2))**2 / (float(len(set1)) * len(set2))

    
def getSimulated(simulated, selected):

    for id2, c2 in enumerate(predicted2):
        maxs=0
        matchedID=-1
        for id1, c1 in enumerate(reference):
            score = matching_score(c1, c2)
            if score>=maxs:
               maxs=score
               matchedID=id1
        abd[matchedID]+=1
        #print matchedID
        #print reference[matchedID]
        for prot in c2:
            if prot in reference[matchedID]:
               #print prot
               protAbd[matchedID][prot]+=1
            #if score > score_threshold:

def readCSV(fname):
	refined_complexes={}
	nline=0
	for line in open(fname):
		if nline>0:
			fields=line.split("\t")
			
			fields[2]=fields[2].replace(" ","")
			prots=fields[2].split(",")
			compl=[]
			for p in prots:
				if p!="":
					compl.append(p)
			refined_complexes[int(fields[0])]=compl
		nline+=1
	return refined_complexes
	
def readSimulated(fname, selected,refined_complexes):

	result = []
	j=0
	complexes={}
	global protAbd
	for line in open(fname):
	
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)
		#print len(ps)
		ps2=[]
		compart=[]
		for prot in ps:
			if "_" in prot:
				ps2.append(prot.split('_')[0])
				compart.append(prot.split('_')[1])
			else:
				ps2.append(prot)
				compart.append("No compartment")
				
		ps=set(ps2)
		compart=set(compart)
		complexes[j]=(ps,compart)
		maxs=-1
		matchedID=-1
		for c1 in refined_complexes.keys():
			score = matching_score(set(refined_complexes[c1]), ps)
			if score>=maxs:
				matchedID=c1
				maxs=score
		if matchedID in selected:
			for prot in ps:
				if prot in protAbd:
					protAbd[prot]+=1
				else:
					protAbd[prot]=1
		j=j+1
	return 0

def readInt(fname):
	
	edges = {}
	nodes = []
	for line in open(fname):
		if line!='':
			prots=line.split('\t')
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			nodes.append(prots[0])
			nodes.append(prots[1])
			if prots[0] in edges:
				edges[prots[0]].append(prots[1])
				edges[prots[0]]=list(set(edges[prots[0]]))
			else:
				edges[prots[0]] = []
				edges[prots[0]].append(prots[1])

			if prots[1] in edges:
				edges[prots[1]].append(prots[0])
				edges[prots[1]]=list(set(edges[prots[1]]))
			else:
				edges[prots[1]] = []
				edges[prots[1]].append(prots[0])
				
	return edges
	
def readAbd(fname):
	
    abd = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print line
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			abd[prots[0]]=float(prots[1])
			
    return abd
	
def networkGeneration(argv):
	#execution: python network_generator.py refined ppi 5(n simulated) simulated1 ... simulated5 id1 id2 ....
	#example: python network_generator.py refined.csv ppi.csv 2 simulated1.txt simulated2.txt 10, 11
	refined_complexes=readCSV(sys.argv[1])
	ppi=readInt(sys.argv[2])
	selected=[]
	ids=sys.argv[4+int(sys.argv[3])]
	ids=ids.split(",")
	for id in ids:
		selected.append(int(id))
		
	simulated=[]
	
	for s in range(int(sys.argv[3])):
		#simulated=simulated+readSimulated(sys.argv[4+s], selected,refined_complexes)
		readSimulated(sys.argv[4+s], selected,refined_complexes)
	if (len(sys.argv)>4+int(sys.argv[3])):
		Abd=readAbd(sys.argv[5+int(sys.argv[3])])
		a=float(sys.argv[6+int(sys.argv[3])])
		b=float(sys.argv[7+int(sys.argv[3])])
	else:
		a=1
		b=1
	
	output_a=open("abundance.txt",'w')
	for p in protAbd:
		output_a.write(p+"\t"+str(protAbd[p])+"\t"+str(protAbd[p]/((Abd[p]*a)**b))+"\n")
	output_a.flush()
	output_a.close()
	output_n=open("network.txt",'w')
	for p1 in protAbd:
		for p2 in (protAbd):
			if p2 in ppi[p1]:
				output_n.write(p1+"\t"+p2+"\n")
	
	output_n.flush()
	output_n.close()	
		


if __name__ == "__main__":
   networkGeneration(sys.argv[1:])
