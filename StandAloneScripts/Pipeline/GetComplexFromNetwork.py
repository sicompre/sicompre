from __future__ import division

import optparse
import sys
import _mysql
import operator
from scipy.stats import hypergeom
from scipy import stats
from textwrap import dedent
import urllib
import numpy as np



def canonical_protein_name(name):
    """Returns the canonical name of a protein by performing a few simple
    transformations on the name."""
    return name.strip().upper()

def is_numeric(x):
    """Returns whether the given string can be interpreted as a number."""
    try:
        float(x)
        return True
    except:
        return False

def matching_score(set1, set2):
    return len(set1.intersection(set2))**2 / (float(len(set1)) * len(set2))
    
def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


def fraction_matched(reference, predicted,  corr, score_threshold=0.25):
	result = 0
	avg={}
	abd = {}
	'''
	for cond in reference:
		abd[cond] = {}
		#for id1, c1 in enumerate(reference[cond]):
			#avg[id1]=0
		for i in predicted[cond]:
			abd_t = {}
			for id1, c1 in enumerate(reference[cond]):
				abd_t[id1] = 0
				#avg[cond][id1]=0
		
			for id2, c2 in enumerate(predicted[cond][i]):
				maxs=0
				matchedID=-1
				for id1, c1 in enumerate(reference[cond]):
					score = matching_score(set(c1), set(c2))
					if score>=maxs:
					   maxs=score
					   matchedID=id1
				abd_t[matchedID]+=1
				#avg[cond][matchedID]+=len(c2)
			abd[cond][i]=abd_t
	'''
	for ic1,cond1 in enumerate(reference):
		for ic2,cond2 in enumerate(reference):
			if ic1>=ic2:
				for id1, c1 in enumerate(reference[cond1]):
					for id2, c2 in enumerate(reference[cond2]):
						if id1!=id2 or ic1!=ic2:
							score = matching_score(set(c1), set(c2))
							if score>=0.2:
								print str(id1)+"_"+str(cond1)+"\t"+str(id2)+"_"+str(cond2)+"\t"+str(score)
								
	#for ic,cond in enumerate(reference):
	#	for id, c in enumerate(reference[cond]):
			#print str(id)+"_"+str(cond)+"\t"+str(cond)+"\t"+str(sum(abd[cond][id])/len(abd[cond]))
			#print str(id)+"_"+str(cond)+"\t"+str(cond)
	
	return 0

def read_complexes(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)

		result.append(ps)
		#if 'Q62318' in ps or 'Q99LI5' in ps:
		#	print len(result)-1
		#	print result[len(result)-1]
	return result

	
def read_complexes2(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	complexes={}
	j=0
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)		
		ps2=[]
		compart=[]
		for prot in ps:
			if '_' in prot:
				ps2.append(prot.split('_')[0])
				compart.append(prot.split('_')[1])
			else:
				ps2.append(prot)
		ps=set(ps2)
		compart=set(compart)
		complexes[j]=(ps,compart)
		j+=1
		if known_proteins is not None:
			isect = ps.intersection(known_proteins)
			if len(isect) < max(min_size, len(ps) * strictness):
				continue
			if len(isect) > max_size:
				continue
			ps = isect
		result.append(ps)

	return result,complexes
    
def read_complexes3(corum_names, fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	counter=0
	compls={}
	for line in open(fname):
		words=line.split('\t')
		words[1]=words[1].replace('\n', '').replace('\r', '')
		if words[1] in compls:
			compls[words[1]].append(words[0])
		else:
			compls[words[1]]=[words[0]]
			
	for i,c in enumerate(compls):
		corum_names[i]=c
		result.append(compls[c])
		
	return result


def read_network(self, fname):
    known_proteins = set()
    for line in open(fname):
        parts = [canonical_protein_name(part) for part in line.strip().split()
                if not is_numeric(part)]
        known_proteins.update(parts)
    return known_proteins
    
def read_matrix(fname):
    edges = {}
    for line in open(fname):
        prots=line.split('\t')
        #print line
        prots[1]=prots[1].replace('\n', '').replace('\r', '')
        if prots[0] in edges:
             if prots[1] in edges[prots[0]]:
                edges[prots[0]][prots[1]]+= 1
             else:
                  edges[prots[0]][prots[1]] = 1
        else:
             edges[prots[0]] = {}
             edges[prots[0]][prots[1]] = 1

        if prots[1] in edges:
             if prots[0] in edges[prots[1]]:
                edges[prots[1]][prots[0]]+= 1
             else:
                  edges[prots[1]][prots[0]] = 1
        else:
             edges[prots[1]] = {}
             edges[prots[1]][prots[0]] = 1
        #known_proteins.update(parts)
    return edges

def read_fun(fname):
    fun = {}
    for line in open(fname):
        words = line.split(' ',1)
        fun[words[0]] = words[1]
    return fun

def generateCSV(argv):

	#example: python generateCSV_20.py ..\refined.txt ..\collins_int.txt ..\fictitious_interactions.txt ..\funcat.txt t ..\CYC08_C1.txt ..\resultCOMPLEX.out2 > test.csv	
	Nconds=int(sys.argv[1])
	n_sims = int(sys.argv[2])
	reference_complexes=[]	
	predicted_complexes={}
	pc={}
	compl=int(sys.argv[3])
	compl_network=sys.argv[4]
	
	nline=0
	complexes={}
	for line in open(compl_network):
		nline+=1
		if nline==compl:
			c=line.replace("\r","").replace("\n","").split("\t")
			for co in c:
				id=int(co.split("_")[0])
				cond=int(co.split("_")[1])
				if cond in complexes:
					complexes[cond].append(id)
				else:
					complexes[cond]=[id]
		
	for i in range(Nconds):
		reference_complexes=read_complexes(sys.argv[5+i])
		if i in complexes:
			for c in complexes[i]:
				for prot in reference_complexes[c]:
					print prot+"\t",
				print ""
		

	return 0


def main(argv):
    return generateCSV(argv)


if __name__ == "__main__":
   main(sys.argv[1:])
