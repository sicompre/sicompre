import optparse
import sys
import operator
import math
import os
import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from random import *
from pylab import *
from Tkinter import *
from tkFileDialog import *
import tkMessageBox
matplotlib.use('TkAgg')

from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
from matplotlib.backend_bases import key_press_handler


from matplotlib.figure import Figure




def readTimeSeries():
	time=0
	for name in os.listdir("."):
		if name.endswith(".out") and name.startswith("Trajectory_Molecule"):
			time=name[len("Trajectory_Molecule"):len("Trajectory_Molecule")+8]
			time=float(time)
			print time
			ts[time]={}
			with open(name) as f:
				for line in f.readlines():
					words=line.replace("\r", "").replace("\n", "").split("\t")
					words[0]=words[0].split("_")[0]
					if words[0] in ts[time]:
						prot=ts[time][words[0]]
					else:
						prot={}
					for i,sv in enumerate(words):
						if i>0 and sv!='':
							abd=sv.split(":")
							abd[0]=int(abd[0])
							if abd[0] in prot:
								prot[abd[0]]=prot[abd[0]]+int(abd[1])
							else:
								prot[abd[0]]=int(abd[1])
					ts[time][words[0]]=prot
					
	return time			
				

def updateMap():
	label.config(text="Time:"+str(var.get()))
	old_t=-1
	x=[]
	y=[]
	
	sel=Lb2.get(0,END)
	#print sel
	#sel=sel.values()
	print sel
	for t in sorted(ts):
		if var.get()<=t and var.get()>old_t:
			for prot in ts[t]:
				if len(sel)==0 or prot in sel:
					for sv in ts[t][prot]:
						for i in range(ts[t][prot][sv]):
							x.append(sv/64)
							y.append(sv%64)
		old_t=t
	#plt.clear()
	#canvas.clear()
	plt.clf()
	hist = plt.hist2d(x,y,bins=64)
	cb = plt.colorbar()
	#cb.update_normal()
	canvas.show()
	#show()

def resetSel():	
	Lb1.selection_clear(Lb1.curselection())
	
def searchProtein():	
	list_p=Lb1.get(0,END)
	index=list_p.index(E1.get())
	Lb1.see(index)
	
def addP():
	sel=Lb1.curselection()
	if len(sel)>0:
		sel=Lb1.get(sel[0])
	Lb2.insert(Lb2.size(),sel)

def removeP():
	Lb2.delete(Lb2.curselection())
	
def loadP():
	fileName = askopenfilename(parent=m2)
	sel=Lb1.get(0,END)
	missing=0
	ln=0
	for line in open(fileName):
		line=line.replace("\r","").replace("\n","")
		ln+=1
		if line in sel:
			Lb2.insert(Lb2.size(),line)	
		else:
			missing+=1
	if missing>0:
		tkMessageBox.showinfo("Warning", "Missing "+str(missing)+" proteins (total "+str(ln)+")")
		

ts={}
max_t = readTimeSeries()
root = Tk()
ind=0
w, h = root.winfo_screenwidth(), root.winfo_screenheight()
# use the next line if you also want to get rid of the titlebar
#root.overrideredirect(1)
root.geometry("%dx%d+0+0" % (w, h))
x = randn(1000)
y = randn(1000)+5
x=[]
y=[]
#tot_mol=0
for t in ts:
	if t==0.0:
		for prot in ts[t]:
			for sv in ts[t][prot]:
				for i in range(ts[t][prot][sv]):
					#tot_mol+=1
					x.append(sv/64)
					y.append(sv%64)
#print tot_mol				
f = plt.figure(figsize=(8,8), dpi=100)
hist = plt.hist2d(x,y,bins=64)
polygon = plt.Polygon([[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]])
cb = plt.colorbar()
#f.add(h)
#a = f.add_subplot(111)
#a.plot(hist2d(x,y,bins=40))
m1 = PanedWindow()
m1.pack(fill=BOTH, expand=1)
m2 = PanedWindow(m1, orient=VERTICAL)
m1.add(m2)
canvas = FigureCanvasTkAgg(f, master=m1)
canvas.show()
canvas.get_tk_widget().pack(anchor=W)
var = DoubleVar()
label = Label(m2)
label.pack(anchor=W)
w = Scale(m2, from_=0, to=max_t, resolution=0.025, length=400, variable=var, orient=HORIZONTAL)
w.pack()
E1 = Entry(m2, bd = 15)
E1.pack()
button_search = Button(m2, text="Search Protein", command=searchProtein)
button_search.pack()
frame = Frame(m2)
scrollbar = Scrollbar(frame, orient=VERTICAL)
Lb1 = Listbox(frame, yscrollcommand=scrollbar.set)
scrollbar.config(command=Lb1.yview)
scrollbar.pack(side=RIGHT, fill=Y)
for t in ts:
	if t==0.0:
		for i,prot in enumerate(ts[t]):
			Lb1.insert(i,prot)
Lb1.pack()
frame.pack()

button_add = Button(m2, text="Track Protein", command=addP)
button_add.pack()
button_add = Button(m2, text="Load Proteins ...", command=loadP)
button_add.pack()

frame2 = Frame(m2)
scrollbar2 = Scrollbar(frame2, orient=VERTICAL)
Lb2 = Listbox(frame2, yscrollcommand=scrollbar.set)
scrollbar2.config(command=Lb2.yview)
scrollbar2.pack(side=RIGHT, fill=Y)
Lb2.pack()
frame2.pack()

button_remove = Button(m2, text="Remove Protein", command=removeP)
button_remove.pack()

button = Button(m2, text="Get Scale Value", command=updateMap)
button.pack()
button_reset = Button(m2, text="Reset Selection", command=resetSel)
button_reset.pack()
root.mainloop()