from __future__ import division

import optparse
import sys
import _mysql
#import pygame
from textwrap import dedent

par=0
int_counter=0
debugCounter=0
debugCounter2=0
usedCompart={}
PC={}
PB={}
PP={}
Sv2Compart={}

def readInt(fname):
	
	edges = {}
	nodes = []
	for line in open(fname):
		if line!='':
			prots=line.split('\t')
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			if len(prots)==4:
				bindingRate=float(prots[2])
				unbindingRate=float(prots[3])
			else:
				bindingRate=1.0
				unbindingRate=1.0
			nodes.append(prots[0])
			nodes.append(prots[1])
			if prots[0] in edges:
				if not prots[1] in edges[prots[0]]:
					edges[prots[0]][prots[1]]=(bindingRate,unbindingRate)
				
			else:
				edges[prots[0]] = {}
				edges[prots[0]][prots[1]]=(bindingRate,unbindingRate)
				
			if prots[1] in edges:
				if not prots[0] in edges[prots[1]]:
					edges[prots[1]][prots[0]]=(bindingRate,unbindingRate)
				
			else:
				edges[prots[1]] = {}
				edges[prots[1]][prots[0]]=(bindingRate,unbindingRate)
				
	return nodes,edges
		
def readProtDom(fname):
	
    edges = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print line
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[1]=prots[1].split('.')[0]
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			if prots[0] in edges:
				if prots[1] in edges[prots[0]]:
					edges[prots[0]][prots[1]]+=1
				else:
					edges[prots[0]][prots[1]]=1
			else:
				edges[prots[0]] = {}
				edges[prots[0]][prots[1]]=1

    return edges
	
def readAssoc(fname):
	
    edges = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print prots
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			if prots[0] in edges:
				edges[prots[0]].append(prots[1])
				edges[prots[0]]=list(set(edges[prots[0]]))
			else:
				edges[prots[0]] = []
				edges[prots[0]].append(prots[1])

    return edges
	
def readAbd(fname):
	
    abd = {}
    diff = {}
    bl = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print line
			prots[-1]=prots[-1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			abd[prots[0]]=float(prots[1])
			if len(prots)==4:
				diff[prots[0]]=float(prots[2])
				bl[prots[0]]=int(prots[3])
			else:
				diff[prots[0]]=1
				bl[prots[0]]=10
			

    return abd,diff,bl

def generateInt(ppi,ddi,P2D,P2C,P2F,strategy,prots):	
	interactions=[]
	prots2=[]
	doneProts=[]
	interDom={}
	fictitiousCounter=0
	normalCounter=0
	debugCounter=0
	f_int=open("fictitious_int.txt",'w')
	for p1 in ppi:
		if p1 in prots:
			for p2 in ppi[p1]:
				if (not (p2 in doneProts)) and (p2 in prots):
					ddiFound=False
					if p1 in P2D:
						for d1 in P2D[p1]:
							if d1 in ddi:
								debugCounter+=1
								if p2 in P2D:
									for d2 in P2D[p2]:
										if d2 in ddi[d1]:
											ddiFound=True
											prots2.append(p1)
											prots2.append(p2)
											interactions.append((d1+'_'+p1,d2+'_'+p2))
											if p1 in interDom:
												interDom[p1].append(d1)
											else:
												interDom[p1]=[d1]
											if p2 in interDom:
												interDom[p2].append(d2)
											else:
												interDom[p2]=[d2]
											normalCounter+=1
												
					if (not ddiFound) and strategy==2:
						funFound=False
						if p1 in P2F:
							for f1 in P2F[p1]:
								if p2 in P2F:
									if f1 in P2F[p2]:
										#if f2 in ddi[d1]:
										funFound=True
						if funFound:
							prots2.append(p1)
							prots2.append(p2)
							interactions.append(('PFX'+str(fictitiousCounter)+"_"+p1,'PFX'+str(fictitiousCounter)+"_"+p2))
							if p1 in interDom:
								interDom[p1].append('PFX'+str(fictitiousCounter))
							else:
								interDom[p1]=['PFX'+str(fictitiousCounter)]
							if p2 in interDom:
								interDom[p2].append('PFX'+str(fictitiousCounter))
							else:
								interDom[p2]=['PFX'+str(fictitiousCounter)]
							fictitiousCounter+=1
							fictitiousCounter+=1
							f_int.write(p1+"\t"+p2+"\n")
							
					elif (not ddiFound) and strategy==1:
						prots2.append(p1)
						prots2.append(p2)
						interactions.append(('PFX'+str(fictitiousCounter)+"_"+p1,'PFX'+str(fictitiousCounter)+"_"+p2))
						if p1 in interDom:
							interDom[p1].append('PFX'+str(fictitiousCounter))
						else:
							interDom[p1]=['PFX'+str(fictitiousCounter)]
						if p2 in interDom:
							interDom[p2].append('PFX'+str(fictitiousCounter))
						else:
							interDom[p2]=['PFX'+str(fictitiousCounter)]
						fictitiousCounter+=1
						f_int.write(p1+"\t"+p2+"\n")
						
		doneProts.append(p1)
	f_int.flush()
	f_int.close()
	print "Fictitious interactions "+str(fictitiousCounter)
	print "Normal interactions "+str(normalCounter)
	print "Debug counter "+str(debugCounter)
	return interactions,list(set(prots2)),interDom
	
def generateAbd(prot,P2C,Abd,M,P, interDom, P2D, diffR, BL):	
	prots=[]
	bindings=[]
	abds=[]
	tmpSet=[]
	P2C_new={}
	global par
	global PC
	global PB
	global PP
	global debugCounter2
	if prot in P2C:
		
		compartments=list(set(P2C[prot]))		

		#print compartments
		
		pAbd=0
		if len(compartments)>0:
			if prot in Abd:
				P2C_new[prot]=compartments
				debugCounter2+=1
				pAbd=(float(Abd[prot])*float(M))**float(P)
				pAbd=pAbd/len(compartments)
				pAbd=int(round(pAbd))
				if pAbd<3:
					pAbd=3
				for c in compartments:
					#prots.append("<speciesType id=\""+prot.replace("-", "")+"_"+c+"\" name=\""+c+"\"/>\n")
					#abds.append("<parameter id=\"d"+str(par)+"\" value=\"1\" />\n")
					#abds.append("<parameter id=\"q"+str(par)+"\" value=\""+str(pAbd)+"\" />\n")
					prots.append(prot+"_"+c+"\t"+c+"\t"+str(pAbd)+"\t"+str(diffR[prot])+"\t"+str(BL[prot])+"\n")
					PC[prot+"_"+c+"\t"+c+"\t"+str(pAbd)+"\t"+str(diffR[prot])+"\t"+str(BL[prot])+"\n"]=(prot,c)
					par+=1
					domlist=""
					for d in list(set(interDom)):
						if prot in P2D:
							if d in P2D[prot]:
								#bindings.append("<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\""+str(P2D[prot][d])+"\" compartment=\"All\"/>\n")
								#PB["<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\""+str(P2D[prot][d])+"\" compartment=\"All\"/>\n"]=(prot,c)
								domlist=domlist+d+"_"+prot+"_"+c+","+str(P2D[prot][d])+";"
							else:
								#bindings.append("<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n")
								#PB["<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n"]=(prot,c)
								domlist=domlist+d+"_"+prot+"_"+c+",1;"
						else:
							#bindings.append("<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n")
							#PB["<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n"]=(prot,c)
							domlist=domlist+d+"_"+prot+"_"+c+",1;"
							
					bindings.append(prot+"_"+c+":"+domlist+"\n")
					PB[prot+"_"+c+":"+domlist+"\n"]=(prot,c)
			
	return prots,bindings,abds,P2C_new
	
def canInteract(c1,c2):	

	ret=True
	if c1==c2:
		return ret
	if (not c1 in Sv2Compart):
		c1=c1.lower()
	if (not c2 in Sv2Compart):
		c2=c2.lower()
	for sv in Sv2Compart[c1]:
		if sv in Sv2Compart[c2]:
			ret=True and ret
		else:
			ret=False
	if ret:
		return True
	ret=True
	for sv in Sv2Compart[c2]:
		if sv in Sv2Compart[c1]:
			ret=True and ret
		else:
			ret=False
	return ret
	
def generateFinalInt(inter,P2C,old1,old2,edges):	
	interactions=[]
	prot1=inter[0].split("_")[1]
	prot2=inter[1].split("_")[1]
	global int_counter
	global debugCounter
	global usedCompart
	alreadyDone=[]
	if (prot1 in P2C) and (prot2 in P2C) and (old1!=prot1 or old2!=prot2):
		debugCounter+=1
		#print prot1+"-"+prot2
		for compart1 in P2C[prot1]:
			for compart2 in P2C[prot2]:
				if (compart1!='null' and compart2!='null'):
					if (canInteract(compart1,compart2)):
						if (not (compart1+compart2 in alreadyDone)) and (not (compart2+compart1 in alreadyDone)):
							alreadyDone.append(compart1+compart2)
							interactions.append(inter[0]+"_"+compart1+"\t"+inter[1]+"_"+compart2+"\t"+str(edges[prot1][prot2][0])+"\t"+str(edges[prot1][prot2][1])+"\n")
							int_counter+=1
							if prot1 in usedCompart:
								usedCompart[prot1].append(compart1)
							else:
								usedCompart[prot1]=[compart1]
							if prot2 in usedCompart:
								usedCompart[prot2].append(compart2)
							else:
								usedCompart[prot2]=[compart2]

	return interactions,prot1,prot2

def readCompart(fname):

	ret = {}
	for line in open(fname):
		comps=line.split(":")
		ret[comps[0]]=[]
		svs=comps[1].replace('\n','').replace('\r','').split(";")
		for sv in svs:
			if sv!='':
				ret[comps[0]].append(int(sv))
	return ret

def modelGeneration(argv):
    #arg1: PPI file separated by \t
    #arg2: DDI file separated by \t
    #arg3: Protein-Domain association in the form Protein1\tDomain1\nProtein2\tDomain1\n
    #arg4: Protein abundances where abundance are expressed in PPM ProteinID\tabd\n
    #arg5: Protein-Compartment association in the form Protein1\tCompartment1\nProtein2\tCompartment1\n
    #arg6: Protein-Function association in the form Protein1\tFunction1\nProtein2\tFunction1\n
    #arg7: Binding Strategy
    #arg10: outfile
    #arg11: compartments
	
	#example> python model_generation.py lit-BM-13_unip.csv iddi.csv human_protdom.csv Kim\bcell_abd.csv human_compart.csv human_fun.csv 2 0.676322 0.7 filecompart.comp 4096 model_bcell_newformat.xml

	prots,ppi=readInt(sys.argv[1])
	prots=list(set(prots))
	print "Prots in PPI "+str(len(prots))
	doms,ddi=readInt(sys.argv[2])
	doms=list(set(doms))
	print "Doms in DDI "+str(len(doms))
	P2D=readProtDom(sys.argv[3])
	Abd,diffR,BL=readAbd(sys.argv[4])
	global Sv2Compart
	if (sys.argv[5]!="none"):
		P2C=readAssoc(sys.argv[5])
		Sv2Compart=readCompart(sys.argv[10])
	else:
		P2C={}
		for p in prots:
			P2C[p]=["ALL"]	
		print "none"
		print len(P2C)
		print sys.argv[11]
		Sv2Compart["ALL"]=list(range(int(sys.argv[11])))
	P2F=readAssoc(sys.argv[6])
	strategy=int(sys.argv[7])
	M=float(sys.argv[8]) #protein abundance multiplier
	P=float(sys.argv[9]) #protein abundance power
	#reference_complexes = read_complexes(sys.argv[1])
	prots=set(prots).intersection(set(Abd.keys()))
	prots=set(prots).intersection(set(P2C.keys()))
	print "Generating interactions..."
	interactionList,prots2,interDom=generateInt(ppi,ddi,P2D,P2C,P2F,strategy,prots)
	print len(interactionList)
	print len(prots2)
	print len(interDom)
	print "Prot2Comp "+str(len(P2C))
	modelProts=[]
	modelBindings=[]
	modelAbds=[]
	modelInteractions=[]
	print "writing proteins information..."
	P2C_new={}
	global usedCompart
	for prot in prots:
		if prot in interDom:
			tmpProts,tmpBindings,tmpAbds,P2C_newb=generateAbd(prot,P2C,Abd,M,P,interDom[prot],P2D, diffR, BL)
			P2C_new=dict(P2C_new.items() + P2C_newb.items())
			modelProts=modelProts+tmpProts
			modelBindings=modelBindings+tmpBindings
			modelAbds=modelAbds+tmpAbds
	print "writing interactions..."
	print "Debug in prots&interDom "+str(int(debugCounter2))	
	P2C=P2C_new
	global debugCounter
	debugCounter=0
	print "P2C "+str(len(P2C))
	old1=""
	old2=""
	for inter in interactionList:
		tmpInteractions,old1,old2=generateFinalInt(inter,P2C,old1,old2,ppi)
		modelInteractions=modelInteractions+tmpInteractions
	
	print "DebugCounter "+str(int(debugCounter))	
	#write on file
	print "writing on file..."
	global PC
	global PP
	global PB
	output=open(sys.argv[12],'w')
	
	output.write("#SiComPre auto-generated model\n\n")
	output.write("@COMPARTMENTS\n")
	for c in Sv2Compart:
		output.write(c.upper()+":")
		for sv in Sv2Compart[c]:
			output.write(str(sv)+";")
		output.write("\n")
	output.write("@MOLECULES\n")
	for a in modelProts:
		if PC[a][0] in usedCompart:
			if PC[a][1] in usedCompart[PC[a][0]]:
				output.write(a)
	output.write("@SITES\n")
	for a in modelBindings:
		if PB[a][0] in usedCompart:
			if PB[a][1] in usedCompart[PB[a][0]]:
				output.write(a)
	output.write("@INTERACTIONS\n")
	for a in modelInteractions:
		output.write(a)
	output.flush()
	output.close()
	print "done!"


	return 0

	
def main(argv):
    return modelGeneration(argv)


if __name__ == "__main__":
   main(sys.argv[1:])
