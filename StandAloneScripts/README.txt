Run SiComPre from terminal:
python runPipeLine.py PPI DDI ProteinDomain ProteinAbundance Compartments ProteinFunction Strategy(1-3) CellVolume AbundanceReduction #SV ModelName MaximumConcentration ReferenceComplexes MergingThr TimePoints #Simulations
-------------------------------
Utils:
convertCompartments.py	convert protein localization associated to GO terms to organelles (e.g. mitochondria and integral to membrane are converted to �cm� which stands for cytoplasm mitochondria membrane). All localization related to complexes, such as ribosome, are deleted.
abundanceMOPED.py		convert files exported from MOPED database to the right format needed for SiComPre.
abundancePAXDB.py		convert files exported from PAX-DB to the right format needed for SiComPre.
