from __future__ import division

import optparse
import sys

from textwrap import dedent


protAbd={}
    
def readAssoc(fname):
	
    edges = {}
	comparts = []
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print prots
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			comparts.append(prots[1])
			if prots[0] in edges:
				edges[prots[0]].append(prots[1].replace('"', ''))
				edges[prots[0]]=list(set(edges[prots[0]]))
			else:
				edges[prots[0]] = []
				edges[prots[0]].append(prots[1].replace('"', ''))

    return edges,list(set(comparts))

def main(argv):

    P2C,comparts=readAssoc(sys.argv[1])
	P2Ctmp={}
	for p in P2C:
		P2Ctmp[p]=[]
		for c in P2C[p]:
			if "plasma membrane" in c.lower() or c.lower()=="cell wall":
				tmpSet.append("pm")
			if c.lower()=="nucleus":
				tmpSet.append("nucleus")
			if c.lower()=="cytoplasm" or c.lower()=="cytosol" or c.lower()=="cytoskeleton" or c.lower()=="punctate composite" or c.lower()=="cell periphery" or c.lower()=="bud":
				tmpSet.append("cytoplasm")
			if c.lower()=="mitochondrion" or c.lower=="mitochondria":
				tmpSet.append("mitochondria")
			if c.lower()=="extracellular space" or c.lower()=="extracellular":
				tmpSet.append("extracellular")
			if c.lower()=="endoplasmic reticulum" or c.lower()=="er":
				tmpSet.append("er")
			if c.lower()=="golgi apparatus" or c.lower()=="golgi":
				tmpSet.append("golgi")
			if c.lower()=="endosome":
				tmpSet.append("endosome")
			if c.lower()=="lysosome":
				tmpSet.append("lysosome")
			if c.lower()=="vesicle" or c.lower()=="transport vesicles" or c.lower()=="vesicles" or c.lower()=="transport vesicle":
				tmpSet.append("vesicles")
			if c.lower()=="peroxisome":
				tmpSet.append("peroxisome")
			if "vacuole" in c.lower():
				tmpSet.append("vacuole")

			if ("mitochond" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cm")
			if ("endoplasmic reticulum" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cer")
			if ("golgi" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cg")
			if ("lysoso" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cl")
			if ("endosome" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cen")
			if ("vesicle" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cve")
			if ("nucl" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cn")
			if ("peroxi" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cp")
			if ("vacuole" in c.lower()) and ("membrane" in c.lower()):
				tmpSet.append("cva")

		
		compartments=list(set(tmpSet))		
		if ("cer" in compartments) and ("cn" in compartments):
			compartments.append("ne")
		
		if ("cm" in compartments and "mitochondria" in compartments):
			compartments.remove("mitochondria")
		
		if ("cer" in compartments and "er" in compartments):
			compartments.remove("er")
		
		if ("cg" in compartments and "golgi" in compartments):
			compartments.remove("golgi")
		
		if ("cl" in compartments and "lysosome" in compartments):
			compartments.remove("lysosome")
		
		if ("cen" in compartments and "endosome" in compartments):
			compartments.remove("endosome")
		
		if ("cve" in compartments and "vesicles" in compartments):
			compartments.remove("vesicles")
		
		if ("cn" in compartments and "nucleus" in compartments):
			compartments.remove("nucleus")
		
		if ("cp" in compartments and "peroxisome" in compartments):
			compartments.remove("peroxisome")
		
		if ("cva" in compartments and "vacuole" in compartments):
			compartments.remove("vacuole")
		
	
	return 0
		


if __name__ == "__main__":
   main(sys.argv[1:])
