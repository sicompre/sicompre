from __future__ import division

import optparse
#import sys
import wx
from textwrap import dedent
from wx.lib.pubsub import pub
import wx


#import pygame
par=0
int_counter=0
debugCounter=0
debugCounter2=0
usedCompart={}
PC={}
PB={}
PP={}
Sv2Compart={}

def readInt(fname):
	
	edges = {}
	nodes = []
	for line in open(fname):
		if line!='':
			prots=line.split('\t')
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			if len(prots)==4:
				bindingRate=float(prots[2])
				unbindingRate=float(prots[3])
			else:
				bindingRate=1.0
				unbindingRate=1.0
			nodes.append(prots[0])
			nodes.append(prots[1])
			if prots[0] in edges:
				if not prots[1] in edges[prots[0]]:
					edges[prots[0]][prots[1]]=(bindingRate,unbindingRate)
				
			else:
				edges[prots[0]] = {}
				edges[prots[0]][prots[1]]=(bindingRate,unbindingRate)
				
			if prots[1] in edges:
				if not prots[0] in edges[prots[1]]:
					edges[prots[1]][prots[0]]=(bindingRate,unbindingRate)
				
			else:
				edges[prots[1]] = {}
				edges[prots[1]][prots[0]]=(bindingRate,unbindingRate)
				
	return nodes,edges
		
def readProtDom(fname):
	
    edges = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print line
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[1]=prots[1].split('.')[0]
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			if prots[0] in edges:
				if prots[1] in edges[prots[0]]:
					edges[prots[0]][prots[1]]+=1
				else:
					edges[prots[0]][prots[1]]=1
			else:
				edges[prots[0]] = {}
				edges[prots[0]][prots[1]]=1

    return edges
	
def readAssoc(fname):
	
    edges = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print prots
			prots[1]=prots[1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			if prots[0] in edges:
				edges[prots[0]].append(prots[1])
				edges[prots[0]]=list(set(edges[prots[0]]))
			else:
				edges[prots[0]] = []
				edges[prots[0]].append(prots[1])

    return edges
	
def readAbd(fname):
	
    abd = {}
    diff = {}
    bl = {}
    for line in open(fname):
		if line!='':
			prots=line.split('\t')
			#print line
			prots[-1]=prots[-1].replace('\n', '').replace('\r', '')
			prots[0]=prots[0].upper()
			prots[1]=prots[1].upper()
			abd[prots[0]]=float(prots[1])
			if len(prots)==4:
				diff[prots[0]]=float(prots[2])
				bl[prots[0]]=int(prots[3])
			else:
				diff[prots[0]]=1
				bl[prots[0]]=10
			

    return abd,diff,bl

def generateInt(ppi,ddi,P2D,P2C,P2F,strategy,prots):	
	interactions=[]
	prots2=[]
	doneProts=[]
	interDom={}
	fictitiousCounter=0
	normalCounter=0
	debugCounter=0
	f_int=open("fictitious_int.txt",'w')
	for p1 in ppi:
		if p1 in prots:
			for p2 in ppi[p1]:
				if (not (p2 in doneProts)) and (p2 in prots):
					ddiFound=False
					if p1 in P2D:
						for d1 in P2D[p1]:
							if d1 in ddi:
								debugCounter+=1
								if p2 in P2D:
									for d2 in P2D[p2]:
										if d2 in ddi[d1]:
											ddiFound=True
											prots2.append(p1)
											prots2.append(p2)
											interactions.append((d1+'_'+p1,d2+'_'+p2))
											if p1 in interDom:
												interDom[p1].append(d1)
											else:
												interDom[p1]=[d1]
											if p2 in interDom:
												interDom[p2].append(d2)
											else:
												interDom[p2]=[d2]
											normalCounter+=1
												
					if (not ddiFound) and strategy==2:
						funFound=False
						if p1 in P2F:
							for f1 in P2F[p1]:
								if p2 in P2F:
									if f1 in P2F[p2]:
										#if f2 in ddi[d1]:
										funFound=True
						if funFound:
							prots2.append(p1)
							prots2.append(p2)
							interactions.append(('PFX'+str(fictitiousCounter)+"_"+p1,'PFX'+str(fictitiousCounter)+"_"+p2))
							if p1 in interDom:
								interDom[p1].append('PFX'+str(fictitiousCounter))
							else:
								interDom[p1]=['PFX'+str(fictitiousCounter)]
							if p2 in interDom:
								interDom[p2].append('PFX'+str(fictitiousCounter))
							else:
								interDom[p2]=['PFX'+str(fictitiousCounter)]
							fictitiousCounter+=1
							fictitiousCounter+=1
							f_int.write(p1+"\t"+p2+"\n")
							
					elif (not ddiFound) and strategy==1:
						prots2.append(p1)
						prots2.append(p2)
						interactions.append(('PFX'+str(fictitiousCounter)+"_"+p1,'PFX'+str(fictitiousCounter)+"_"+p2))
						if p1 in interDom:
							interDom[p1].append('PFX'+str(fictitiousCounter))
						else:
							interDom[p1]=['PFX'+str(fictitiousCounter)]
						if p2 in interDom:
							interDom[p2].append('PFX'+str(fictitiousCounter))
						else:
							interDom[p2]=['PFX'+str(fictitiousCounter)]
						fictitiousCounter+=1
						f_int.write(p1+"\t"+p2+"\n")
						
		doneProts.append(p1)
	f_int.flush()
	f_int.close()
	print "Fictitious interactions "+str(fictitiousCounter)
	print "Normal interactions "+str(normalCounter)
	print "Debug counter "+str(debugCounter)
	return interactions,list(set(prots2)),interDom
	
def generateAbd(prot,P2C,Abd,M,P, interDom, P2D, diffR, BL):	
	prots=[]
	bindings=[]
	abds=[]
	tmpSet=[]
	P2C_new={}
	global par
	global PC
	global PB
	global PP
	global debugCounter2
	if prot in P2C:
		
		compartments=list(set(P2C[prot]))		

		#print compartments
		
		pAbd=0
		if len(compartments)>0:
			if prot in Abd:
				P2C_new[prot]=compartments
				debugCounter2+=1
				pAbd=(float(Abd[prot])*float(M))**float(P)
				pAbd=pAbd/len(compartments)
				pAbd=int(round(pAbd))
				if pAbd<3:
					pAbd=3
				for c in compartments:
					#prots.append("<speciesType id=\""+prot.replace("-", "")+"_"+c+"\" name=\""+c+"\"/>\n")
					#abds.append("<parameter id=\"d"+str(par)+"\" value=\"1\" />\n")
					#abds.append("<parameter id=\"q"+str(par)+"\" value=\""+str(pAbd)+"\" />\n")
					prots.append(prot+"_"+c+"\t"+c+"\t"+str(pAbd)+"\t"+str(diffR[prot])+"\t"+str(BL[prot])+"\n")
					PC[prot+"_"+c+"\t"+c+"\t"+str(pAbd)+"\t"+str(diffR[prot])+"\t"+str(BL[prot])+"\n"]=(prot,c)
					par+=1
					domlist=""
					for d in list(set(interDom)):
						if prot in P2D:
							if d in P2D[prot]:
								#bindings.append("<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\""+str(P2D[prot][d])+"\" compartment=\"All\"/>\n")
								#PB["<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\""+str(P2D[prot][d])+"\" compartment=\"All\"/>\n"]=(prot,c)
								domlist=domlist+d+"_"+prot+"_"+c+","+str(P2D[prot][d])+";"
							else:
								#bindings.append("<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n")
								#PB["<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n"]=(prot,c)
								domlist=domlist+d+"_"+prot+"_"+c+",1;"
						else:
							#bindings.append("<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n")
							#PB["<species id=\""+d+"_"+prot+"_"+c+"\" speciesType=\""+prot+"_"+c+"\" name=\"Specie "+d+"_"+prot+"_"+c+"\" initialAmount=\"1\" compartment=\"All\"/>\n"]=(prot,c)
							domlist=domlist+d+"_"+prot+"_"+c+",1;"
							
					bindings.append(prot+"_"+c+":"+domlist+"\n")
					PB[prot+"_"+c+":"+domlist+"\n"]=(prot,c)
			
	return prots,bindings,abds,P2C_new
	
def canInteract(c1,c2):	

	ret=True
	if c1==c2:
		return ret
	if (not c1 in Sv2Compart):
		c1=c1.lower()
	if (not c2 in Sv2Compart):
		c2=c2.lower()
	for sv in Sv2Compart[c1]:
		if sv in Sv2Compart[c2]:
			ret=True and ret
		else:
			ret=False
	if ret:
		return True
	ret=True
	for sv in Sv2Compart[c2]:
		if sv in Sv2Compart[c1]:
			ret=True and ret
		else:
			ret=False
	return ret
	
def generateFinalInt(inter,P2C,old1,old2,edges):	
	interactions=[]
	prot1=inter[0].split("_")[1]
	prot2=inter[1].split("_")[1]
	global int_counter
	global debugCounter
	global usedCompart
	alreadyDone=[]
	if (prot1 in P2C) and (prot2 in P2C) and (old1!=prot1 or old2!=prot2):
		debugCounter+=1
		#print prot1+"-"+prot2
		for compart1 in P2C[prot1]:
			for compart2 in P2C[prot2]:
				if (compart1!='null' and compart2!='null'):
					if (canInteract(compart1,compart2)):
						if (not (compart1+compart2 in alreadyDone)) and (not (compart2+compart1 in alreadyDone)):
							alreadyDone.append(compart1+compart2)
							interactions.append(inter[0]+"_"+compart1+"\t"+inter[1]+"_"+compart2+"\t"+str(edges[prot1][prot2][0])+"\t"+str(edges[prot1][prot2][1])+"\n")
							int_counter+=1
							if prot1 in usedCompart:
								usedCompart[prot1].append(compart1)
							else:
								usedCompart[prot1]=[compart1]
							if prot2 in usedCompart:
								usedCompart[prot2].append(compart2)
							else:
								usedCompart[prot2]=[compart2]

	return interactions,prot1,prot2

def readCompart(fname):
	print fname
	ret = {}
	for line in open(fname):
		comps=line.split(":")
		ret[comps[0]]=[]
		svs=comps[1].split(";")
		print comps[0]
		print comps[1]
		for sv in svs:
			if sv.replace("\r","").replace("\n","")!='':
				ret[comps[0]].append(int(sv))
	return ret

def modelGeneration(argv,modelSummary,progressBar,progressCounter):
    #arg1: PPI file separated by \t
    #arg2: DDI file separated by \t
    #arg3: Protein-Domain association in the form Protein1\tDomain1\nProtein2\tDomain1\n
    #arg4: Protein abundances where abundance are expressed in PPM ProteinID\tabd\n
    #arg5: Protein-Compartment association in the form Protein1\tCompartment1\nProtein2\tCompartment1\n
    #arg6: Protein-Function association in the form Protein1\tFunction1\nProtein2\tFunction1\n
    #arg7: Binding Strategy
    #arg10: outfile
    #arg11: compartments
	
	#example> python model_generation.py lit-BM-13_unip.csv iddi.csv human_protdom.csv Kim\bcell_abd.csv human_compart.csv human_fun.csv 2 0.676322 0.7 filecompart.comp 4096 model_bcell_newformat.xml
	
	wx.CallAfter(pub.sendMessage, "summary", msg=str(argv))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	prots,ppi=readInt(argv[1])
	prots=list(set(prots))
	wx.CallAfter(pub.sendMessage, "summary", msg="Prots in PPI "+str(len(prots)))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	doms,ddi=readInt(argv[2])
	doms=list(set(doms))
	wx.CallAfter(pub.sendMessage, "summary", msg= "Doms in DDI "+str(len(doms)))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	P2D=readProtDom(argv[3])
	Abd,diffR,BL=readAbd(argv[4])
	
	progressCounter+=1
	wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
        
	global Sv2Compart
	if (argv[10]=="default"):
		P2C=readAssoc(argv[5])
		vacuole=False
		for p in P2C:
			if "VACUOLE" in P2C[p]:
				vacuole=True
				Sv2Compart["vacuole"]=[]
				Sv2Compart["cva"]=[]
		Sv2Compart["extracellular"]=[]
		Sv2Compart["cytoplasm"]=[]
		Sv2Compart["pm"]=[]
		Sv2Compart["cn"]=[]
		Sv2Compart["er"]=[]
		Sv2Compart["ne"]=[]
		Sv2Compart["nucleus"]=[]
		Sv2Compart["cer"]=[]
		Sv2Compart["golgi"]=[]
		Sv2Compart["cg"]=[]
		Sv2Compart["mitochondria"]=[]
		Sv2Compart["cm"]=[]
		Sv2Compart["vesicles"]=[]
		Sv2Compart["cve"]=[]
		Sv2Compart["peroxisome"]=[]
		Sv2Compart["cp"]=[]
		Sv2Compart["lysosome"]=[]
		Sv2Compart["cl"]=[]
		Sv2Compart["endosome"]=[]
		Sv2Compart["cen"]=[]
		
		sizeGrid=argv[11].split("x")
		sizeGridX=int(sizeGrid[0])
		sizeGridY=int(sizeGrid[1])
		
		for i in range(sizeGridX*sizeGridY):
			if (i<64*2 or i%64<2 or i%64>61 or i>=64*62) :
				#extracellular space
				Sv2Compart["extracellular"].append(i)
				if (i>=64*1 or i%64==1 or i%64==62 or i<64*63) :
					Sv2Compart["cytoplasm"].append(i)
					Sv2Compart["pm"].append(i)
			elif ( (i<64*61 and i>=64*50)  and  (i%64<59 and i%64>4) ) :
				#nucleous
				Sv2Compart["nucleus"].append(i)
				#nuclear membrane with cytoplasm
				if (i>=64*60 or i<64*51 or i%64==58 or i%64==5) :
					if (not (i<64*51 and ((i%64<30 and i%64>13) or (i%64<51 and i%64>34)))) :
						Sv2Compart["cytoplasm"].append(i)
						#CN
						Sv2Compart["cn"].append(i)
					else :
						
						Sv2Compart["er"].append(i)
						#NE
						Sv2Compart["ne"].append(i)
				#nuclear membrane with er1
			elif ( ( (i<64*51 and i>=64*48)    and  (i%64<30 and i%64>13)) or ( (i<64*51 and i>=64*48)    and  (i%64<51 and i%64>34))) :  #nuclear-er bridges 
				#is nucleous
				Sv2Compart["nucleus"].append(i)
				#is er
				Sv2Compart["er"].append(i)
				#is NE
				Sv2Compart["ne"].append(i)
				if ((i<64*50 and i>64*49) and (i%64==22 or i%64==21 or i%64==43 or i%64==42)) :
					#is C-N-Er
					Sv2Compart["cytoplasm"].append(i)
			elif (  ( (i<64*49) and (i>=64*46) )  and  (i%64<60 and i%64>3)   or   ( (i<64*45) and (i>=64*42) )  and  (i%64<60 and i%64>3)  ) :
				#er
				Sv2Compart["er"].append(i)
				if ((i>=64*48 and i<64*49) or (i<64*47 and i>=64*46) or i%64==59 or i%64==4 or (i>=64*44 and i<64*45) or (i<64*43 and i>=64*42)) :
					Sv2Compart["cytoplasm"].append(i)
					#CEr
					Sv2Compart["cer"].append(i)

			elif (      ( (i<64*40) and (i>=64*36) )  and  (i%64<59 and i%64>4)     ) :
				#golgi
				Sv2Compart["golgi"].append(i)
				if (i>=64*39 or i<64*37 or i%64==5 or i%64==58) :
					Sv2Compart["cytoplasm"].append(i)
					#CG
					Sv2Compart["cg"].append(i)

			elif (      ( (i<64*35) and (i>=64*18) )  and  (i%64<57 and i%64>7)     ) :
				#mitochondria
				#split mitochondria in 5
				if ((i % 64) % 10 == 7) :#is just cytosol (no mitochondria)
					Sv2Compart["cytoplasm"].append(i)

				elif ((i % 64) % 10 == 6 or (i % 64) % 10 == 8) : #is membrane
					
					Sv2Compart["mitochondria"].append(i)

					Sv2Compart["cytoplasm"].append(i)
					#CM
					Sv2Compart["cm"].append(i)
				
				else :
					Sv2Compart["mitochondria"].append(i)
					if (i>=64*34 or i<64*19 or i%64==8 or i%64==56) :
						Sv2Compart["cytoplasm"].append(i)
						#CM
						Sv2Compart["cm"].append(i)
			elif (      ( (i<64*16) and (i>=64*12) )  and  (i%64<28 and i%64>2)     ) :
				#vesicles
				#split vesicles in 4
				if ((i % 64) % 5 == 3) :#is just cytosol (no vesicles)
					
					Sv2Compart["cytoplasm"].append(i)
				elif ((i % 64) % 5 == 2 or (i % 64) % 5 == 4) : #is membrane
					
					Sv2Compart["vesicles"].append(i)

					Sv2Compart["cytoplasm"].append(i)
					#CVe
					Sv2Compart["cve"].append(i)
				else :
					Sv2Compart["vesicles"].append(i)
					if (i>=64*15 or i<64*13 or i%64==27 or i%64==3) :
						Sv2Compart["cytoplasm"].append(i)
						#CVe
						Sv2Compart["cve"].append(i)

			elif (      ( (i<64*16) and (i>=64*12) )  and  (i%64<60 and i%64>32)     ) :
				#peroxisomes
				Sv2Compart["peroxisome"].append(i)
				if (i>=64*15 or i<64*13 or i%64==59 or i%64==33) :
					Sv2Compart["cytoplasm"].append(i)
					#CP
					Sv2Compart["cp"].append(i)

			elif (      ( (i<64*11) and (i>=64*7) )  and  (i%64<26 and i%64>5)     ) :
				#lysosome
				Sv2Compart["lysosome"].append(i)
				if (i>=64*10 or i<64*8 or i%64==25 or i%64==6) :
					Sv2Compart["cytoplasm"].append(i)
					#CL
					Sv2Compart["cl"].append(i)

			elif (      ( (i<64*11) and (i>=64*7) )  and  (i%64<60 and i%64>28)     ) :
				#endosome
				Sv2Compart["endosome"].append(i)
				if (i>=64*10 or i<64*8 or i%64==59 or i%64==29) :
					Sv2Compart["cytoplasm"].append(i)
					#CEn
					Sv2Compart["cen"].append(i)

			elif (vacuole and ( ( (i<64*6) and (i>=64*3) )  and  (i%64<51 and i%64>12) )) :
				
				Sv2Compart["vacuole"].append(i)
				if (i>=64*5 or i<64*4 or i%64==50 or i%64==13) :
					Sv2Compart["cytoplasm"].append(i)
					#CVa
					Sv2Compart["cva"].append(i)

			else :
				#cytoplasm
				Sv2Compart["cytoplasm"].append(i)
	
	elif (argv[5]!="none"):
		P2C=readAssoc(argv[5])
		Sv2Compart=readCompart(argv[10])
	else:
		sizeGrid=argv[11].split("x")
		sizeGridX=int(sizeGrid[0])
		sizeGridY=int(sizeGrid[1])
		P2C={}
		for p in prots:
			P2C[p]=["ALL"]	
		wx.CallAfter(pub.sendMessage, "summary", msg="none")
		wx.CallAfter(pub.sendMessage, "summary", msg="\n")
		wx.CallAfter(pub.sendMessage, "summary", msg=str(len(P2C)))
		wx.CallAfter(pub.sendMessage, "summary", msg="\n")
		wx.CallAfter(pub.sendMessage, "summary", msg=str(argv[11]))
		wx.CallAfter(pub.sendMessage, "summary", msg="\n")
		Sv2Compart["ALL"]=list(range(sizeGridX*sizeGridY))
		wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	
	progressCounter+=1
	wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
	
	P2F=readAssoc(argv[6])
	strategy=int(argv[7])
	M=float(argv[8]) #protein abundance multiplier
	P=float(argv[9]) #protein abundance power
	#reference_complexes = read_complexes(argv[1])
	prots=set(prots).intersection(set(Abd.keys()))
	prots=set(prots).intersection(set(P2C.keys()))
	wx.CallAfter(pub.sendMessage, "summary", msg="Generating interactions...")
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	interactionList,prots2,interDom=generateInt(ppi,ddi,P2D,P2C,P2F,strategy,prots)
	wx.CallAfter(pub.sendMessage, "summary", msg=str(len(interactionList)))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	wx.CallAfter(pub.sendMessage, "summary", msg= str(len(prots2)))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	wx.CallAfter(pub.sendMessage, "summary", msg= str(len(interDom)))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	wx.CallAfter(pub.sendMessage, "summary", msg= "Prot2Comp "+str(len(P2C)))
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	modelProts=[]
	modelBindings=[]
	modelAbds=[]
	modelInteractions=[]
	wx.CallAfter(pub.sendMessage, "summary", msg= "writing proteins information...")
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	P2C_new={}
	global usedCompart
	for prot in prots:
		if prot in interDom:
			tmpProts,tmpBindings,tmpAbds,P2C_newb=generateAbd(prot,P2C,Abd,M,P,interDom[prot],P2D, diffR, BL)
			P2C_new=dict(P2C_new.items() + P2C_newb.items())
			modelProts=modelProts+tmpProts
			modelBindings=modelBindings+tmpBindings
			modelAbds=modelAbds+tmpAbds
	
	progressCounter+=1
	wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
	wx.CallAfter(pub.sendMessage, "summary", msg= "writing interactions...")
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	wx.CallAfter(pub.sendMessage, "summary", msg= "Debug in prots&interDom "+str(int(debugCounter2)))	
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	P2C=P2C_new
	global debugCounter
	debugCounter=0
	wx.CallAfter(pub.sendMessage, "summary", msg= "P2C "+str(len(P2C)))
	old1=""
	old2=""
	for inter in interactionList:
		tmpInteractions,old1,old2=generateFinalInt(inter,P2C,old1,old2,ppi)
		modelInteractions=modelInteractions+tmpInteractions
	
	progressCounter+=1
	wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
	#print "DebugCounter "+str(int(debugCounter))	
	#write on file
	wx.CallAfter(pub.sendMessage, "summary", msg="writing on file...")
	wx.CallAfter(pub.sendMessage, "summary", msg="\n")
	global PC
	global PP
	global PB
	#if (argv[5]!="none"):
	#	output=open(argv[12],'w')
	#else:
	#	output=open(argv[11],'w')
	output=open(argv[12],'w')
	output.write("#SiComPre auto-generated model\n\n")
	output.write("#Size:"+argv[11]+"\n")
	output.write("@COMPARTMENTS\n")
	for c in Sv2Compart:
		output.write(c.upper()+":")
		for sv in Sv2Compart[c]:
			output.write(str(sv)+";")
		output.write("\n")
	output.write("@MOLECULES\n")
	for a in modelProts:
		if PC[a][0] in usedCompart:
			if PC[a][1] in usedCompart[PC[a][0]]:
				output.write(a)
	output.write("@SITES\n")
	for a in modelBindings:
		if PB[a][0] in usedCompart:
			if PB[a][1] in usedCompart[PB[a][0]]:
				output.write(a)
	output.write("@INTERACTIONS\n")
	for a in modelInteractions:
		output.write(a)
	output.flush()
	output.close()
	
	progressCounter+=1
	wx.CallAfter(pub.sendMessage, "update", msg=progressCounter)
	wx.CallAfter(pub.sendMessage, "summary", msg= "done!")


	return 0

