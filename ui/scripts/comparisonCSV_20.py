from __future__ import division

#import _mysql
import numpy as np
import operator
import optparse
#from scipy import stats
#from scipy.stats import hypergeom
import sys
from textwrap import dedent
#import urllib


def canonical_protein_name(name):
    """Returns the canonical name of a protein by performing a few simple
    transformations on the name."""
    return name.strip().upper()

def is_numeric(x):
    """Returns whether the given string can be interpreted as a number."""
    try:
        float(x)
        return True
    except:
        return False

def matching_score(set1, set2):
    return len(set1.intersection(set2))**2 / (float(len(set1)) * len(set2))
    
def addslashes(s):
    d = {'"':'\\"', "'":"\\'", "\0":"\\\0", "\\":"\\\\"}
    return ''.join(d.get(c, c) for c in s)


def fraction_matched(referenceA,referenceB, predictedA,predictedB, abd, edges, edgesF, corr, corum, idcorum,tot_timepoints, allProts, score_threshold=0.25):
	result = 0
	avgA={}
	abdA = {}
	for id1, c1 in enumerate(referenceA):
		avgA[id1]=0
	for i in predictedA:
		abd_t = {}
		for id1, c1 in enumerate(referenceA):
			abd_t[id1] = 0
			avgA[id1]=0
	
		for id2, c2 in enumerate(predictedA[i]):
			maxs=0
			matchedID=-1
			for id1, c1 in enumerate(referenceA):
				score = matching_score(c1, c2)
				if score>=maxs:
				   maxs=score
				   matchedID=id1
			abd_t[matchedID]+=1
			avgA[matchedID]+=len(c2)
		abdA[i]=abd_t
		
	avgB={}
	abdB = {}
	missingB=[]
	for id1, c1 in enumerate(referenceB):
		avgB[id1]=0
		missingB.append(id1)
	for i in predictedA:
		abd_t = {}
		for id1, c1 in enumerate(referenceB):
			abd_t[id1] = 0
			avgB[id1]=0
	
		for id2, c2 in enumerate(predictedB[i]):
			maxs=0
			matchedID=-1
			for id1, c1 in enumerate(referenceB):
				score = matching_score(c1, c2)
				if score>=maxs:
				   maxs=score
				   matchedID=id1
			abd_t[matchedID]+=1
			avgB[matchedID]+=len(c2)
		abdB[i]=abd_t
	print 'Complex ID cond. A\tSize\tList of proteins cond. A\tQuantitative Prediction cond. A\tComplex ID cond. B\tSize\tList of proteins cond. B\tQuantitative Prediction cond. B\tT-value\tP-value\tCond. A - B matching\tBest matching reference complex cond. A\tMatching cond. A\tLength of reference complex cond. A\tBest matching reference complex cond. B\tMatching cond. B\tLength of reference complex cond. B'
	variance=False
	if corr=="t":
		variance=True
	else:
		variance=False
	for id1, c1 in enumerate(referenceA):
		tot_int=0
		tot_f=0
		print ''+str(id1)+'\t'+str(len(c1))+'\t',
		protL=""
		for prot in c1:
			print prot+",",
		tot_abd=0.0
		dist=[]
		for i in predictedA:
			tot_abd+=abdA[i][id1]
			dist.append(abdA[i][id1])
		print '\t'+str(tot_abd/len(predictedA))+'\t',
		maxs=(100000000,0)
		matchedID=-1
		matchedC=[]
		distC=[]
		tot_abdB=0
		bestS=0
		for id2,c2 in enumerate(referenceB):
			score = matching_score(c1, c2)
			if score>0.2:
				tot_abdt=0.0
				distB=[]
				for i in predictedB:
					tot_abdt+=abdB[i][id2]
					distB.append(abdB[i][id2])
				val=stats.ttest_ind(np.array(dist), np.array(distB), equal_var = variance)
				#print '\t'+str(dist)+str(distB)+str(val)+'\t',
				if abs(val[0])<abs(maxs[0]):
					maxs=val
					matchedID=id2
					matchedC=c2
					tot_abdB=tot_abdt
					bestS=score
		if matchedID!=-1:			
			print ''+str(matchedID)+'\t'+str(len(matchedC))+'\t',
			for prot in matchedC:
				print prot+",",
			print '\t'+str(tot_abdB/len(predictedB))+'\t',		
			print str(abs(maxs[0]))+'\t'+str(maxs[1])+'\t',
			if matchedID in missingB:
				missingB.remove(matchedID)
			print bestS,
		else:
			print "\t\t\t\t\t\t",
		maxs=-1
		mlen=0
		for id2, c2 in enumerate(corum):
			score = matching_score(c1, c2)
			if score>=maxs:
				maxs=score
				matchedID=id2
				mlen=len(c2)
		if maxs>0:
			print '\t'+idcorum[matchedID]+'\t',
			print "%.4f" % (maxs),
			print "\t"+str(mlen),
		else:
			print '\t\t\t',
		maxs=-1
		mlen=0
		if len(matchedC)>0:
			for id2, c2 in enumerate(corum):
				score = matching_score(set(matchedC), set(c2))
				if score>=maxs:
					maxs=score
					matchedID=id2
					mlen=len(c2)
		if maxs>0:
			print '\t'+idcorum[matchedID]+'\t',
			print "%.4f" % (maxs),
			print "\t"+str(mlen),
			print "\t",
		else:
			print '\t\t\t\t',
		#print '\t',
		n=len(c1)
		fun = {}
		tot=0
		#print protL
		#f=urllib.request.urlopen(url="david.abcc.ncifcrf.gov/api.jsp?type=ENSEMBL_GENE_ID&ids="+protL+"&tool=chartReport&annot=GOTERM_MF_ALL")
		#print f
		#return
		'''
		for prot in c1:
		db.query("SELECT fun FROM human_functions WHERE IDprot='"+prot+"'")
		r=db.store_result()
		res=r.fetch_row(maxrows=0,how=1)
		for row in res:
			if row['fun'] in fun:
			   fun[row['fun']]+=1
			   tot+=1
			else:
			   fun[row['fun']]=1
			   tot+=1
		print '\t',
		pvalue={}
		for f in fun:
			k=fun[f]
			pvalue[f]=hypergeom.sf(k-1,int(NN),int(m[f]),n)
			#print hypergeom.cdf(1,6128,216,62)
		sorted_x = sorted(pvalue.iteritems(), key=operator.itemgetter(1))
		#sorted_x.reverse()
		fs=[]
		for k,v in sorted_x:
			fs.append(k)
		sorted_x=fs
		for f in sorted_x:
			if pvalue[f]<=0.1:
				print str(funschema[f]).replace('\n', '')+':',
				print "%.4g\t" % (pvalue[f]),
				#print str(pvalue[f])+',',
				#print str(fun[f])+'\t'+str(NN)+'\t'+str(m[f])+'\t',
		'''
		
		print ''
	for i in missingB:
		print "\t\t\t\t",
		print ''+str(i)+'\t'+str(len(referenceB[i]))+'\t',
		for prot in referenceB[i]:
			print prot+",",
		tot_abd=0.0
		for t in predictedB:
			tot_abd+=abdB[t][i]
		print '\t'+str(tot_abd/len(predictedB))+'\t',		
		print '\t\t',
		print '\t\t\t\t',
		maxs=-1
		mlen=0
		if len(referenceB[i])>0:
			for id2, c2 in enumerate(corum):
				score = matching_score(set(referenceB[i]), set(c2))
				if score>=maxs:
					maxs=score
					matchedID=id2
					mlen=len(c2)
		if maxs>0:
			print '\t'+idcorum[matchedID]+'\t',
			print "%.4f" % (maxs),
			print "\t"+str(mlen),
		else:
			print '\t\t\t',
		print ''
	return 0

def read_complexes(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)

		result.append(ps)
		
	return result

	
def read_complexes2(fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	complexes={}
	j=0
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)		
		ps2=[]
		compart=[]
		for prot in ps:
			if '_' in prot:
				ps2.append(prot.split('_')[0])
				compart.append(prot.split('_')[1])
			else:
				ps2.append(prot)
		ps=set(ps2)
		compart=set(compart)
		complexes[j]=(ps,compart)
		j+=1
		if known_proteins is not None:
			isect = ps.intersection(known_proteins)
			if len(isect) < max(min_size, len(ps) * strictness):
				continue
			if len(isect) > max_size:
				continue
			ps = isect
		result.append(ps)

	return result,complexes
    
def read_complexes3(corum_names, fname, known_proteins=None, strictness=0,
        min_size=1, max_size=2000):
	result = []
	counter=0
	compls={}
	for line in open(fname):
		words=line.split('\t')
		words[1]=words[1].replace('\n', '').replace('\r', '')
		if words[1] in compls:
			compls[words[1]].append(words[0])
		else:
			compls[words[1]]=[words[0]]
			
	for i,c in enumerate(compls):
		corum_names[i]=c
		result.append(compls[c])
		
	return result


def read_network(self, fname):
    known_proteins = set()
    for line in open(fname):
        parts = [canonical_protein_name(part) for part in line.strip().split()
                if not is_numeric(part)]
        known_proteins.update(parts)
    return known_proteins
    
def read_matrix(fname):
    edges = {}
    for line in open(fname):
        prots=line.split('\t')
        #print line
        prots[1]=prots[1].replace('\n', '').replace('\r', '')
        if prots[0] in edges:
             if prots[1] in edges[prots[0]]:
                edges[prots[0]][prots[1]]+= 1
             else:
                  edges[prots[0]][prots[1]] = 1
        else:
             edges[prots[0]] = {}
             edges[prots[0]][prots[1]] = 1

        if prots[1] in edges:
             if prots[0] in edges[prots[1]]:
                edges[prots[1]][prots[0]]+= 1
             else:
                  edges[prots[1]][prots[0]] = 1
        else:
             edges[prots[1]] = {}
             edges[prots[1]][prots[0]] = 1
        #known_proteins.update(parts)
    return edges

def read_fun(fname):
    fun = {}
    for line in open(fname):
        words = line.split(' ',1)
        fun[words[0]] = words[1]
    return fun

def comparisonCSV(argv):

	#example: python generateCSV_20.py ..\refined.txt ..\collins_int.txt ..\fictitious_interactions.txt ..\funcat.txt t ..\CYC08_C1.txt ..\resultCOMPLEX.out2 > test.csv	
	
	reference_complexes_A = read_complexes(sys.argv[1])
	reference_complexes_B = read_complexes(sys.argv[2])	
	n_sims = int(sys.argv[3])	
	edges={}
	edgesF={}
	fun=read_fun(sys.argv[4])
	#strategy = sys.argv[5]
	corr=sys.argv[5]
	idToName2 = {}
	corum = read_complexes3(idToName2,sys.argv[6])
	#corumOriginal = read_complexes3(idToName2,sys.argv[7])

	#if strategy=='n':
	#	strategy=''
	abdd = {}
	#for i, c1 in enumerate(reference_complexes):
	#	abdd[i]=0

	predicted_complexes_A={}
	pc_A={}
	predicted_complexes_B={}
	pc_B={}
	tot_timepoints=0
	for i,f in enumerate(sys.argv):
		if i>6:
			if tot_timepoints<n_sims:
				predicted_complexes2,pc2 = read_complexes2(sys.argv[i])
				predicted_complexes_A[i-6]=predicted_complexes2
				pc_A=dict(pc_A.items() + pc2.items())
				tot_timepoints+=1
			else:
				predicted_complexes2,pc2 = read_complexes2(sys.argv[i])
				predicted_complexes_B[i-6-n_sims]=predicted_complexes2
				pc_B=dict(pc_B.items() + pc2.items())
				
	allProts=[]
	'''
	for i in predicted_complexes:
		for c in predicted_complexes[i]:
			allProts=allProts+list(c)
	allProts=list(set(allProts))
	'''
	fraction_matched(reference_complexes_A, reference_complexes_B, predicted_complexes_A, predicted_complexes_B, abdd, edges, edgesF, corr, corum,idToName2,tot_timepoints,allProts)

	return 0


def main(argv):
    return comparisonCSV(argv)


if __name__ == "__main__":
   main(sys.argv[1:])
