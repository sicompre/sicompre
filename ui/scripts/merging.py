import math
import numpy as np
from scipy.stats import hypergeom
import sys


def canonical_protein_name(name):
    """Returns the canonical name of a protein by performing a few simple
    transformations on the name."""
    return name.strip().upper()
	
def read_complexes(fname):
	result = []
	for line in open(fname):
		ps = set(canonical_protein_name(x) for x in line.strip().split() if x)		
		ps2=[]
		
		for prot in ps:
			if "_" in prot:
				ps2.append(prot.split('_')[0])
			else:
				ps2.append(prot)
				
		ps=set(ps2)
		if len(ps)>0:
			result.append(ps)
			
	#result=list(set(result))
	print "finding duplicates"
	#result = list(set(frozenset(i) for i in result))
	
	print "removing duplicates"
	#result = [r for i, r in enumerate(result) if i not in to_delete]
	
	return result


def merging(argv):
    
    complexes=read_complexes(argv[1])
    Thr=float(argv[2])
    print "merging "+str(len(complexes))+"..."
    complexes.sort(lambda x,y: cmp(len(x), len(y)))
    mthr=max(0.9,Thr)
    while mthr>=0.5 and mthr>Thr:
    	print mthr
    	for id1,s1 in enumerate(complexes):
    		for id2,s2 in enumerate(complexes):
    			if id1<id2:
    				if len(s1)>0 and len(s2)>0:
    					#if abs(len(s1)-len(s2))/max(len(s1),len(s2))<0.3:
    					if len(s2)/len(s1)>=mthr:
    						if float(float(len(s1.intersection(s2))**2)/float(len(s1)*len(s2)))>=mthr:
    							tmp=set(list(s1)+list(s2))
    							print s1
    							print s2
    							print tmp
    							if len(tmp)<150:
    								#s1=tmp
    								complexes[id1]=tmp
    								complexes[id2]=[]
    					else:
    						#print abs(len(s1)-len(s2))
    						break
    		
    	complexes_new=[]
    	for c in complexes:
    		if len(c)>0:
    			complexes_new.append(c)
    
    	complexes=complexes_new
    	mthr=max(mthr-0.1,Thr)
    
    m=0
    #Thr=0.5
    print complexes
    overlapping=np.zeros((len(complexes),len(complexes)))
    c_id=0
    max_overlapping=0.0
    
    print "merging "+str(len(complexes))+"..."
    mer_counter=0
    
    for i,s1 in enumerate(complexes):
    	for j,s2 in enumerate(complexes):
    		if (i>j) :
    			overlap=float(float(len(s1.intersection(s2))**2)/float(len(s1)*len(s2)))
    			if m==0:
    				a=float(len(s1)*len(s2))
    				if a < 4:
    					a=4
    				if a > 100:
    					a=100
    				overlap= overlap   *   (  (  (a - 4)  / (100-4)  ) * 2.5 + 1)
    
    				
    			overlapping[i,j]=overlap
    			overlapping[j,i]=overlap
    			
    			if (overlap>max_overlapping):
    				max_overlapping=overlap;
    c_id=len(complexes)
    			   
    print "overlap done"
    removed=0
    #start first merging
    print "max"+str(max_overlapping)
    max_overlapping=np.amax(overlapping);
    im=np.argmax(overlapping)
    i=int(math.floor(im/len(complexes)))
    j=int(im % len(complexes))
    #overlapping_tmp=np.zeros((len(complexes),len(complexes)))
    while (max_overlapping>Thr):
    	print "overlapping: "+str(max_overlapping)+" size:"+str(len(complexes))+" removed:"+str(removed)
    	merged=False
    	if (overlapping[i,j]==max_overlapping):
    		merged=True;
    		tmp2=list(complexes[j]);
    		tmp2=tmp2+list(complexes[i])
    		hs=set(tmp2)
    		
    		overlapping[i,j]=0;
    		overlapping[j,i]=0;
    		if len(tmp2)<150:
    			equal1=False
    			if set(complexes[i])== hs:
    				equal1=True
    			equal2=False
    			if set(complexes[j]) == hs :
    				equal2=True
    			if (not equal1):
    				removed+=1
    				complexes[i]=[]
    				overlapping[i,:]=overlapping[i,:]*0
    				overlapping[:,i]=overlapping[:,i]*0
    				
    			if (not equal2):
    				removed+=1
    				complexes[j]=[]
    				overlapping[j,:]=overlapping[j,:]*0
    				overlapping[:,j]=overlapping[:,j]*0
    				
    			if (not equal1 and not equal2):
    				tmp2=list(hs)
    				equal=False
    				for d_tmp in complexes:
    					if set(d_tmp) == set(tmp2):
    						equal=True
    				if (not equal):
    					complexes.append(set(tmp2));
    					#rebuild overlapping
    					#overlapping_tmp=np.array(overlapping);
    					#overlapping_tmp=np.array(overlapping);
    					overlapping=np.append(overlapping, np.zeros((len(complexes)-1,1), dtype=float), axis=1)
    					overlapping=np.append(overlapping, np.zeros((1,len(complexes)), dtype=float), axis=0)
    					#overlapping=np.zeros((len(complexes),len(complexes)))
    					#overlapping=
    					#overlapping=np.add(overlapping,overlapping_tmp)
    					k=c_id
    					overlapping[k,:]=overlapping[k,:]*0
    					overlapping[:,k]=overlapping[:,k]*0
    					for l in range(len(complexes)):
    						
    						if (l!=c_id):
    							r=l;
    							if len(complexes[r])>0:
    								overlap=float(float(len(set(tmp2).intersection(complexes[r]))**2)/float(len(tmp2)*len(complexes[r])))
    								if m==0:
    									a=float(len(s1)*len(s2))
    									if a < 4:
    										a=4
    									if a > 100:
    										a=100
    									overlap= overlap   *   (  (  (a - 4)  / (100-4)  ) * 2.5 + 1)
    								
    								if (overlap>max_overlapping):
    									max_overlapping=overlap;
    								overlapping[k,l]=overlap
    								overlapping[l,k]=overlap
    							
    								
    					c_id+=1
    	else:
    		print "error"
    	
    	
    	if removed>2000:
    		removed=0
    		complexes_new=[]
    		for c in complexes:
    			if len(c)>0:
    				complexes_new.append(c)
    
    		complexes=complexes_new
    		
    		overlapping=np.zeros((len(complexes),len(complexes)))
    		
    		for i,s1 in enumerate(complexes):
    			for j,s2 in enumerate(complexes):
    				if (i>j) :
    					overlap=float(float(len(s1.intersection(s2))**2)/float(len(s1)*len(s2)))
    					if m==0:
    						a=float(len(s1)*len(s2))
    						if a < 4:
    							a=4
    						if a > 100:
    							a=100
    						overlap= overlap   *   (  (  (a - 4)  / (100-4)  ) * 2.5 + 1)
    
    						
    					overlapping[i,j]=overlap
    					overlapping[j,i]=overlap
    					
    		c_id=len(complexes)
    		
    	#find new maximum
    	max_overlapping=np.amax(overlapping);
    	im=np.argmax(overlapping)
    	i=int(math.floor(im/len(complexes)))
    	j=int(im % len(complexes))
    	
    '''
    allProts=[]
    max_s=0
    new_compl=0;
    new_complexes=[]
    for c in complexes:
    	new_compl+=1
    	if len(c)>0:	
    		new_complexes.append(c)
    		allProts=allProts+list(c)
    		allProts=list(set(allProts))
    complexes=new_complexes
    
    overlapping=np.ones((len(complexes),len(complexes)))	#dtype=float64
    c_id=0
    max_overlapping=1.0
    
    print "merging..."
    mer_counter=0
    Thr=0.01
    lap=len(allProts)
    for i,s1 in enumerate(complexes):
    	ls1=len(s1)
    	for j,s2 in enumerate(complexes):
    		if (i>j) :
    			k=len(s1.intersection(s2))
    			overlap=min(hypergeom.sf(k-1,lap,len(s2),ls1),hypergeom.sf(k-1,lap,ls1,len(s2)))
    			overlapping[i,j]=overlap
    			overlapping[j,i]=overlap
    			
    			if (overlap<max_overlapping):
    				max_overlapping=overlap;
    
    c_id=len(complexes)
    			   
    print "overlap done"
    removed=0
    #start first merging
    print "max"+str(max_overlapping)
    max_overlapping=np.amin(overlapping);
    im=np.argmin(overlapping)
    i=int(math.floor(im/len(complexes)))
    j=int(im % len(complexes))
    print i
    print j
    print overlapping[i,j]
    print overlapping
    #overlapping_tmp=np.zeros((len(complexes),len(complexes)))
    while (max_overlapping<Thr):
    	print "overlapping: "+str(max_overlapping)+" size:"+str(len(complexes))+" removed:"+str(removed)
    	merged=False
    	if (overlapping[i,j]==max_overlapping):
    		merged=True;
    		tmp2=list(complexes[j]);
    		tmp2=tmp2+list(complexes[i])
    		hs=set(tmp2)
    		
    		overlapping[i,j]=0;
    		overlapping[j,i]=0;
    		
    		equal1=False
    		if set(complexes[i])== hs:
    			equal1=True
    		equal2=False
    		if set(complexes[j]) == hs :
    			equal2=True
    		if (not equal1):
    			removed+=1
    			complexes[i]=[]
    			overlapping[i,:]=np.ones((1, len(complexes)))[0,:]
    			overlapping[:,i]=np.ones((len(complexes), 1))[:,0]
    			
    		if (not equal2):
    			removed+=1
    			complexes[j]=[]
    			overlapping[j,:]=np.ones((1, len(complexes)))[0,:]
    			overlapping[:,j]=np.ones((len(complexes), 1))[:,0]
    			
    		if (not equal1 and not equal2):
    			tmp2=list(hs)
    			equal=False
    			for d_tmp in complexes:
    				if set(d_tmp) == set(tmp2):
    					equal=True
    			if (not equal):
    				complexes.append(tmp2);
    				#rebuild overlapping
    				#overlapping_tmp=np.array(overlapping);
    				#overlapping_tmp=np.array(overlapping);
    				overlapping=np.append(overlapping, np.ones((len(complexes)-1,1), dtype=float), axis=1)
    				overlapping=np.append(overlapping, np.ones((1,len(complexes)), dtype=float), axis=0)
    				#overlapping=np.zeros((len(complexes),len(complexes)))
    				#overlapping=
    				#overlapping=np.add(overlapping,overlapping_tmp)
    				k=c_id
    				overlapping[k,:]=np.ones((1, len(complexes)))[0,:]
    				overlapping[:,k]=np.ones((len(complexes), 1))[:,0]
    				for l in range(len(complexes)):
    					
    					if (l!=c_id):
    						r=l;
    						if len(complexes[r])>0:
    							k=len(s1.intersection(s2))
    							overlap=min(hypergeom.sf(k-1,len(allProts),len(s2),len(s1)),hypergeom.sf(k-1,len(allProts),len(s1),len(s2)))
    							
    							if (overlap>max_overlapping):
    								max_overlapping=overlap;
    							overlapping[k,l]=overlap
    							overlapping[l,k]=overlap
    						
    				c_id+=1
    	else:
    		print "error"
    	
    	#find new maximum
    	max_overlapping=np.amin(overlapping);
    	im=np.argmin(overlapping)
    	i=int(math.floor(im/len(complexes)))
    	j=int(im % len(complexes))
    			
    '''			
    
    print "writing complexes..."
    out_file=open("refined_complexes.txt","w")
    max_s=0
    new_compl=0;
    for c in complexes:
    	new_compl+=1
    	if len(c)>0:
    		for p in c:
    			out_file.write(p+"\t")
    		out_file.write("\n")
    out_file.flush()
    out_file.close()
