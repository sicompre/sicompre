# SiComPre

SiComPre is a software which simulates protein-protein interactions on a large scale to predict protein complexes.

The software can be executed from a GUI and runs on both windows and linux. Input samples are provided in the Data directory of this repository.



Results of this project are published in

Rizzetto S, Moyseos P, Baldacci B, Priami C, Csik�sz-Nagy A. Context Dependent Prediction of Protein Complexes by SiComPre. Systems Biology and Applications. 2018.



Details of the original method are described in

Rizzetto S, Priami C, Csik�sz-Nagy A. Qualitative and quantitative protein complex prediction through proteome-wide simulations. PLoS computational biology. 2015 Oct 22;11(10):e1004424.

# Usage
To execute SiComPre is enough to execute the SiComPre.exe file in windows or the SiComPre binary file in the dist_linux directory